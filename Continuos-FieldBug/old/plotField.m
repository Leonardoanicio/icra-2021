function [X] = plotField(alpha1, alpha2, P, x, y, z, points)
    dx = (x(2) - x(1))/(points - 1);
    dy = (y(2) - y(1))/(points - 1);
    [xx, yy] = meshgrid(x(1):dx:x(2), y(1):dy:y(2));
    u = zeros(size(x));
    v = zeros(size(y));
    for i=1:size(xx, 1)
        for j=1:size(xx, 2)
            x = xx(i, j);
            y = yy(i, j);
            dx = AlphaField(alpha1, alpha2, P, [x; y; z]);
            dx(1:2) = dx(1:2)/(1e-12 + norm(dx(1:2)));
            u(i, j) = dx(1);
            v(i, j) = dx(2);
        end
    end
    quiver(xx, yy, u, v);
end