function [dx, D] = AlphaField(alpha1, alpha2, P, x0)
        dc = 1e-6;
        
        g_a1 = [alpha1(x0(1) + dc, x0(2), x0(3)) - alpha1(x0(1), x0(2), x0(3));
                alpha1(x0(1), x0(2) + dc, x0(3)) - alpha1(x0(1), x0(2), x0(3));
                alpha1(x0(1), x0(2), x0(3) + dc) - alpha1(x0(1), x0(2), x0(3));];
            
        g_a2 = [alpha2(x0(1) + dc, x0(2), x0(3)) - alpha2(x0(1), x0(2), x0(3));
                alpha2(x0(1), x0(2) + dc, x0(3)) - alpha2(x0(1), x0(2), x0(3));
                alpha2(x0(1), x0(2), x0(3) + dc) - alpha2(x0(1), x0(2), x0(3));];
            
        g_P = [P(x0(1) + dc, x0(2), x0(3)) - P(x0(1), x0(2), x0(3));
               P(x0(1), x0(2) + dc, x0(3)) - P(x0(1), x0(2), x0(3));
               P(x0(1), x0(2), x0(3) + dc) - P(x0(1), x0(2), x0(3));];
          
        G = -(2/pi)*atan(3*sqrt(P(x0(1), x0(2), x0(3))));
        H = sqrt(1-G^2);
           
        D = norm(P(x0(1), x0(2), x0(3)));
        
        dx = G*g_P/(1e-9 + norm(g_P)) - H*cross(g_a1, g_a2)/(1e-9 + norm(cross(g_a1, g_a2)));
        dx = dx/norm(dx);
end

