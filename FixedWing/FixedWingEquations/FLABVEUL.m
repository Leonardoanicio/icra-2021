function [Alpha, Beta] = FLABVEUL(states, C, UAV)
    [pn, pe, h, u, v, w, phi, theta, psi, p, q, r] = deal(states{:});
  
    
    fx_ex = C(1);
    fy_ex = C(2);
    fz_ex = C(3);
    l_ex = C(4);
    m_ex = C(5);
    n_ex = C(6);
    
    vt = norm([u,v,w]);
    
    g = 9.81;
    
    Delta = [
        -w/(UAV.uavMass*(u^2 + w^2)), 0., u/(UAV.uavMass*(u^2 + w^2)), 0., 0., 0.;
        -u/(vt*UAV.uavMass*sqrt(u^2 + w^2)), (vt - v)/(vt*UAV.uavMass*sqrt(u^2 + w^2)), -w/(vt*UAV.uavMass*sqrt(u^2 + w^2)), 0., 0., 0.;
        u/(UAV.uavMass*vt), v/(UAV.uavMass*vt), w/(UAV.uavMass*vt), 0., 0., 0.;
        0., 0., 0., (cos(phi)*sin(theta)*UAV.Jxz + UAV.Jz*cos(theta))/(cos(theta)*(UAV.Jx*UAV.Jz - UAV.Jxz^2)), sin(phi)*sin(theta)/(UAV.Jy*cos(theta)), (cos(phi)*sin(theta)*UAV.Jx + UAV.Jxz*cos(theta))/(cos(theta)*(UAV.Jx*UAV.Jz - UAV.Jxz^2))
        0., 0., 0., -sin(phi)*UAV.Jxz/(UAV.Jx*UAV.Jz - UAV.Jxz^2), cos(phi)/UAV.Jy, -sin(phi)*UAV.Jx/(UAV.Jx*UAV.Jz - UAV.Jxz^2);
        0., 0., 0., cos(phi)*UAV.Jxz/(cos(theta)*(UAV.Jx*UAV.Jz - UAV.Jxz^2)), sin(phi)/(cos(theta)*UAV.Jy), cos(phi)*UAV.Jx/(cos(theta)*(UAV.Jx*UAV.Jz - UAV.Jxz^2));
       ];
    
    B = [
       (cos(phi)*cos(theta)*g*u*UAV.uavMass + sin(theta)*g*UAV.uavMass*w + (-p*u*v + q*u^2 + q*w^2 - r*v*w)*UAV.uavMass + fz_ex*u - fx_ex*w)/((u^2 + w^2)*UAV.uavMass);
       (-UAV.uavMass*g*((-vt + v)*sin(phi) + cos(phi)*w)*cos(theta) + sin(theta)*g*u*UAV.uavMass + vt*(p*w - r*u)*UAV.uavMass - fy_ex*v + fy_ex*vt - fz_ex*w - fx_ex*u)/(sqrt(u^2 + w^2)*vt*UAV.uavMass);
       (g*UAV.uavMass*(cos(phi)*w + sin(phi)*v)*cos(theta) - sin(theta)*g*u*UAV.uavMass + fx_ex*u + fy_ex*v + fz_ex*w)/(vt*UAV.uavMass);
       ((-2*q*r*(UAV.Jx*UAV.Jz - UAV.Jxz^2)*cos(phi)^2 - sin(phi)*(q - r)*(q + r)*(UAV.Jx*UAV.Jz - UAV.Jxz^2)*cos(phi) - q*(UAV.Jxz*p - UAV.Jz*r)*UAV.Jy - 2*q*r*UAV.Jxz^2 + (p*(UAV.Jx + UAV.Jz)*q + n_ex)*UAV.Jxz + (q*(UAV.Jx - UAV.Jz)*r + l_ex)*UAV.Jz)*UAV.Jy*cos(theta)^2 - sin(theta)*(-(-q*(UAV.Jx*p - UAV.Jxz*r)*UAV.Jy + (-q*(UAV.Jx + UAV.Jz)*r + l_ex)*UAV.Jxz + (p*(UAV.Jx + UAV.Jz)*q + n_ex)*UAV.Jx)*UAV.Jy*cos(phi) + (UAV.Jx*UAV.Jz - UAV.Jxz^2)*(p*r*UAV.Jy + UAV.Jxz*(p^2 - r^2) + p*(UAV.Jx - UAV.Jz)*r - m_ex)*sin(phi))*cos(theta) + (4*UAV.Jx*UAV.Jz - 4*UAV.Jxz^2)*(cos(phi)^2*q*r + sin(phi)*(1/2*q^2 - 1/2*r^2)*cos(phi) - 1/2*q*r)*UAV.Jy)/(cos(theta)^2*(UAV.Jx*UAV.Jz - UAV.Jxz^2)*UAV.Jy);
       ((-(UAV.Jx*UAV.Jz - UAV.Jxz^2)*(p*r*UAV.Jy + UAV.Jxz*(p^2 - r^2) + p*r*UAV.Jx - p*r*UAV.Jz - m_ex)*cos(phi) - (-q*(UAV.Jx*p - UAV.Jxz*r)*UAV.Jy + (-UAV.Jx*q*r - UAV.Jz*q*r + l_ex)*UAV.Jxz + UAV.Jx*(UAV.Jx*p*q + UAV.Jz*p*q + n_ex))*UAV.Jy*sin(phi))*cos(theta) + (UAV.Jx*UAV.Jz - UAV.Jxz^2)*sin(theta)*UAV.Jy*((q^2 - r^2)*cos(phi)^2 - 2*sin(phi)*q*r*cos(phi) - q^2))/(cos(theta)*(UAV.Jx*UAV.Jz - UAV.Jxz^2)*UAV.Jy);
       ((-(UAV.Jx*UAV.Jz - UAV.Jxz^2)*(p*r*UAV.Jy + UAV.Jxz*(p^2 - r^2) + p*r*UAV.Jx - p*r*UAV.Jz - m_ex)*sin(phi) + cos(phi)*(-q*(UAV.Jx*p - UAV.Jxz*r)*UAV.Jy + (-UAV.Jx*q*r - UAV.Jz*q*r + l_ex)*UAV.Jxz + UAV.Jx*(UAV.Jx*p*q + UAV.Jz*p*q + n_ex))*UAV.Jy)*cos(theta) + (4*UAV.Jx*UAV.Jz - 4*UAV.Jxz^2)*sin(theta)*(sin(phi)*(1/2*q^2 - 1/2*r^2)*cos(phi) + q*r*(cos(phi)^2 - 1/2))*UAV.Jy)/(cos(theta)^2*(UAV.Jx*UAV.Jz - UAV.Jxz^2)*UAV.Jy);
       ];
    
    Beta = inv(Delta);
    Alpha = -Beta*B;
end