function [fC, O] = curveFunction(curve_type)
    switch curve_type
        case 1 %circle
            fC = @(x,t) ([cos(x); sin(x); 0*x]);
        case 2 %race track
            fC = @(x,t) ((x>=0 & x<pi/2) .* [cos(2*x+pi/2)-1; sin(2*x+pi/2); 0*x]) + ...
            ((x>=pi/2 & x<pi) .* [4*x/pi-3; 0*x-1; 0*x]) +...
            ((x>=pi & x<3*pi/2) .* [cos(2*x-pi/2)+1; sin(2*x-pi/2); 0*x])+...
            ((x>=3*pi/2 & x<=2*pi) .* [-4*x/pi+7; 0*x+1; 0*x]);
        case 3 %square
            fC = @(x,t) ((x>=0 & x<pi/2) .* [0*x-1; -4*x/pi+1; 0*x]) + ...
            ((x>=pi/2 & x<pi) .* [4*x/pi-3; 0*x-1; 0*x]) +...
            ((x>=pi & x<3*pi/2) .* [0*x+1; 4*x/pi-5; 0*x])+...
            ((x>=3*pi/2 & x<=2*pi) .* [-4*x/pi+7; 0*x+1; 0*x]);
        case 4 %smooth square
            fC = @(x,t) ((x>=0 & x<pi) .* [-2/pi*x+1; -(1-(-2/pi*x+1).^4).^(1/4); 0*x]) + ...
            ((x>=pi & x<=2*pi) .* [2/pi*x-3; (1-(2/pi*x-3).^4).^(1/4); 0*x]);
        case 5 %crown
            fC = @(x,t) ([cos(x); sin(x); cos(7*x)/2]);
        case 6 %vertical circle
            fC = @(x,t) ([0*x; sin(x); cos(x)]);
        case 7 %crazy loops
            fC = @(x,t) ([cos(4*x).*cos(x); sin(x); 0*x]);
        case 8 %polar rose
            fC = @(x,t) ([sin(2*x).*cos(x); sin(2*x).*sin(x); 0*x]);
        case 9
            w = 250;w2 = 150;
            fC = @(x,t) [r_x + 0.3*sin(t/w); r_y - 0.3*cos(t/w); 0].*([cos(x); sin(x); 0*x]) +...
                [c_x + 0.25*sin(t/w2); c_y - 0.25*cos(t/w2); 0];
            %curve.fC = fC;
            return
        case 10
            w = 100;
            fC = @(x,t)([r_x.*cos(x) + c_x; r_y.*sin(x) + c_y; r_z.*cos(t/w) + c_z + 0*x]);
            return
        case 11 % Lemniscata (VANT)
            a = 600; b = 40;
            fC = @(x,t) ([a*cos(x)./(1 + sin(x).^2);...
                a*sin(x).*cos(x)./(1+sin(x).^2);...
                250 + b*cos(x-pi/2)]);
            O0 = [370 212 200 25]';
            O1 = [200 -170 200 25]';
            O2 = [-150 -130 200 25]';
            O3 = [-570 -20 200 25]';
            O4 = [-590 -10 200 25]';
            O5 = [-600 0 200 25]';
            O6 = [-610 10 200 25]';
            O7 = [-620 20 200 25]';
            O8 = [-630 30 200 25]';
            O9 = [-640 40 200 25]';
            O = [O0 O1 O2 O3 O4 O5 O6 O7 O8 O9];
            return
    end
end