function graf = plot_alpha(curve_type)
[X, Y, Z] = meshgrid(linspace(-700,700,200), linspace(-700, 700, 200), linspace(175, 300, 100));

% W = X.^2 ./a_curve^2 + Z.^2 ./ c_curve^2 - 1;
% Y = 2.*b_curve.*X.*Z./(a_curve.*c_curve);
% W = X.^2 + Z.^2  - 1;
% Y = 2.*X.*Z;
switch curve_type
    case 1
        a = 600;
        b = 400;
        u = 50;
        W = (X./a).^2 + (Y./b).^2 - 1;
        Z = 200 - u*((X/a).^2 - 1);
        
    case 3
        a = 600;
        b = 100*a^3;
        c = 1e4;
        W = (X.^2 + Y.^2).*(Y.^2 + X.*(X+a)) - 4.*a.*X.*Y.*Y - b;
        Z = 200 + (X.^2 + Y.^2)/c;
end
        
sup = isosurface(X, Y, Z, W, 0);
graf = patch(sup);
%view(3);
end