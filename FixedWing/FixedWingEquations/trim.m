function [states, controls, xdot] = trim(states, controls, plotinfo, UAV, Wind)
    warning('off')
    
    Q = diag([0 0 0 1 1 1 1 1 1 0 0 0]);
    
    %Upper bounds (controls + x < ub)
    ub = UAV.UpperBounds - controls;
    %Lower bounds (controls + x > lb)
    lb = UAV.LowerBounds - controls;
    
    %States lower bounds (u + x > 20) (theta + x > -pi/3)
    slb = -ones(12, 1)*inf;
    slb(1:3) = 0;
    slb(4) = 20 - states(4);
    slb(8) = -pi/3 - states(8);
    slb(9) = 0;
    slb(10:12) = 0;
    %States upper bounds (u + x < 25) (theta + x < pi/3)
    sub = ones(12, 1)*inf;
    sub(1:3) = 0;
    sub(4) = 25 - states(4);
    sub(8) = pi/3 - states(8);
    sub(9) = 0;
    sub(10:12) = 0;
    
    options = optimoptions('fmincon', 'MaxFunEvals', 1e4, 'TolCon', 1e-12, 'Display', 'notify');
    
    x = fmincon(@(x)obj(x, states, controls, Q, UAV, Wind), zeros(16, 1), [], [], [], [],...
                            [slb; lb], [sub; ub], @(x)nonlcon(x, states, controls, UAV, Wind), options);
                        
    states = states + x(1:12);
    controls = controls + x(13:16);
    states = num2cell(states);
    
    [pn, pe, pd, u, v, w, phi, theta, psi, p, q, r] = states{:};
    h = -pd;
    
    %Alpha, Beta, Va
    alpha = atan2(w,u);
    vt = norm([u,v,w]);
    beta = asin(v/vt);
    
    alphadot = 0;

    %Controles atuais
    ail = controls(1);
    elev = controls(2);
    rud = controls(3);
    
    %Calculo da densidade do ar
    temperatura = 288.15 - 0.0065*h;
    rho = 1.225*(temperatura/288.15)^(-9.81/(-0.0065*287.04)-1);

    %Matrizes de forças
    Coeffs_C = coeffs_C(alpha, beta, vt, p, q, r, alphadot, rho, UAV);
    Coeffs_M = coeffs_M(alpha, beta, vt, ail, elev, rud, rho, UAV);

    xdot = EOM(states, Coeffs_M*controls + Coeffs_C, UAV, Wind); 
    states = cell2mat(states);
end

function [c, ceq] = nonlcon(x, states, controls, UAV, Wind)
    vt_min = 18;
    vt_max = 28;
    alpha_min = -0.0349;
    alpha_max = 0.279;
    beta_min = -0.05;
    beta_max = 0.05;
    
    states = states + x(1:12);
    controls = controls + x(13:16);
    states = num2cell(states);
    c = [];
    ceq = [];
    
    [pn, pe, pd, ur, vr, wr, phi, theta, psi, p, q, r] = states{:};
    h = -pd;
    
    %Alpha, Beta, Va
    alpha = atan2(wr,ur);
    vt = norm([ur,vr,wr]);
    beta = asin(vr/vt);
    
    alphadot = 0;

    %Controles atuais
    ail = controls(1);
    elev = controls(2);
    rud = controls(3);
    
    %Calculo da densidade do ar
    temperatura = 288.15 - 0.0065*h;
    rho = 1.225*(temperatura/288.15)^(-9.81/(-0.0065*287.04)-1);

    %Matrizes de forças
    Coeffs_C = coeffs_C(alpha, beta, vt, p, q, r, alphadot, rho, UAV);
    Coeffs_M = coeffs_M(alpha, beta, vt, ail, elev, rud, rho, UAV);

    xdot = EOM(states, Coeffs_M*controls + Coeffs_C, UAV, Wind);   
    %vt > vt_min
    c = [c; vt_min - vt];
    %vt < vt_max
    c = [c; vt - vt_max];
    %beta < beta_max
    c = [c; beta - beta_max];
    %beta > beta_min
    c = [c; beta_min - beta];
    %alpha < alpha_max
    c = [c; alpha - alpha_max];
    %alpha > alpha_min
    c = [c; alpha_min - alpha];
    %Estados = 0
    ceq = [phi; p; q; r];
    %Derivadas = 0
    ceq = [ceq; xdot(4:12)];
end

function [fval] = obj(x, states, controls, Q, UAV, Wind)
    states = states + x(1:12);
    controls = controls + x(13:16);

    states = num2cell(states);

    [pn, pe, pd, ur, vr, wr, phi, theta, psi, p, q, r] = states{:};
    h = -pd;
   
    %Alpha, Beta, Va
    alpha = atan2(wr,ur);
    vt = norm([ur,vr,wr]);
    beta = asin(vr/vt);

    alphadot = 0;

    %Controles atuais
    ail = controls(1);
    elev = controls(2);
    rud = controls(3);

    %Calculo da densidade do ar
    temperatura = 288.15 - 0.0065*h;
    rho = 1.225*(temperatura/288.15)^(-9.81/(-0.0065*287.04)-1);

    %Matrizes de forças
    Coeffs_C = coeffs_C(alpha, beta, vt, p, q, r, alphadot, rho, UAV);
    Coeffs_M = coeffs_M(alpha, beta, vt, ail, elev, rud, rho, UAV);

    xdot = EOM(states, Coeffs_M*controls + Coeffs_C, UAV, Wind);
    
    fval = xdot'*Q*xdot;
end