function [Alpha, Beta] = FLVEULdot(states, C, UAV)
    [pn, pe, h, u, v, w, phi, theta, psi, p, q, r] = deal(states{:});
    
    fx_ex = C(1);
    fy_ex = C(2);
    fz_ex = C(3);
    l_ex = C(4);
    m_ex = C(5);
    n_ex = C(6);
    
    vt = norm([u,v,w]);
    
    g = 9.81;
    
    Delta = [
        u / UAV.uavMass / vt v / UAV.uavMass / vt 0.1e1 / UAV.uavMass * w / vt 0 0 0;
        0 0 0 (sin(theta) * UAV.Jxz * cos(phi) + UAV.Jz * cos(theta)) / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) / cos(theta) 0.1e1 / UAV.Jy * sin(theta) / cos(theta) * sin(phi) (sin(theta) * UAV.Jx * cos(phi) + UAV.Jxz * cos(theta)) / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) / cos(theta);
        0 0 0 -UAV.Jxz / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) * sin(phi) 0.1e1 / UAV.Jy * cos(phi) -UAV.Jx / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) * sin(phi);
        0 0 0 UAV.Jxz / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) / cos(theta) * cos(phi) 0.1e1 / UAV.Jy / cos(theta) * sin(phi) UAV.Jx / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) / cos(theta) * cos(phi);
        ];

    
    B = [
        (g * UAV.uavMass * (cos(phi) * w + sin(phi) * v) * cos(theta) - sin(theta) * g * u * UAV.uavMass + fx_ex * u + fy_ex * v + fz_ex * w) / vt / UAV.uavMass;
        ((-0.2e1 * q * r * (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) * cos(phi) ^ 2 - sin(phi) * (q - r) * (q + r) * (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) * cos(phi) - q * (UAV.Jxz * p - UAV.Jz * r) * UAV.Jy - 0.2e1 * q * r * UAV.Jxz ^ 2 + (p * (UAV.Jx + UAV.Jz) * q + n_ex) * UAV.Jxz + UAV.Jz * (q * (UAV.Jx - UAV.Jz) * r + l_ex)) * UAV.Jy * cos(theta) ^ 2 - (-(-q * (UAV.Jx * p - UAV.Jxz * r) * UAV.Jy + (-q * (UAV.Jx + UAV.Jz) * r + l_ex) * UAV.Jxz + UAV.Jx * (p * (UAV.Jx + UAV.Jz) * q + n_ex)) * UAV.Jy * cos(phi) + (p * r * UAV.Jy + UAV.Jxz * (p ^ 2 - r ^ 2) + p * (UAV.Jx - UAV.Jz) * r - m_ex) * sin(phi) * (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2)) * sin(theta) * cos(theta) + 0.4e1 * (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) * UAV.Jy * (cos(phi) ^ 2 * q * r + sin(phi) * (q ^ 2 / 0.2e1 - r ^ 2 / 0.2e1) * cos(phi) - q * r / 0.2e1)) / cos(theta) ^ 2 / UAV.Jy / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2);
        ((-(p * r * UAV.Jy + UAV.Jxz * (p ^ 2 - r ^ 2) + UAV.Jx * p * r - UAV.Jz * p * r - m_ex) * (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) * cos(phi) - (-q * (UAV.Jx * p - UAV.Jxz * r) * UAV.Jy + (-UAV.Jx * q * r - UAV.Jz * q * r + l_ex) * UAV.Jxz + UAV.Jx * (UAV.Jx * p * q + UAV.Jz * p * q + n_ex)) * sin(phi) * UAV.Jy) * cos(theta) + ((q ^ 2 - r ^ 2) * cos(phi) ^ 2 - 0.2e1 * sin(phi) * q * r * cos(phi) - q ^ 2) * (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) * UAV.Jy * sin(theta)) / cos(theta) / UAV.Jy / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2);
        ((-(p * r * UAV.Jy + UAV.Jxz * (p ^ 2 - r ^ 2) + UAV.Jx * p * r - UAV.Jz * p * r - m_ex) * (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) * sin(phi) + (-q * (UAV.Jx * p - UAV.Jxz * r) * UAV.Jy + (-UAV.Jx * q * r - UAV.Jz * q * r + l_ex) * UAV.Jxz + UAV.Jx * (UAV.Jx * p * q + UAV.Jz * p * q + n_ex)) * UAV.Jy * cos(phi)) * cos(theta) + 0.4e1 * (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) * UAV.Jy * sin(theta) * (sin(phi) * (q ^ 2 / 0.2e1 - r ^ 2 / 0.2e1) * cos(phi) + q * r * (cos(phi) ^ 2 - 0.1e1 / 0.2e1))) / cos(theta) ^ 2 / UAV.Jy / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2)];

    Beta = (Delta')/(Delta*Delta');
    Alpha = -Beta*B;
end