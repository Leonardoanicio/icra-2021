close all
clear
clc

%Simulacao
dt = 0.01;
tf = 200;

%MPC
nu = 2; 
nx = 10;

%Escolhe o campo
curve_type = 1;

%%%%%%
%Escolhe o feedback
feedType = "ABVEULdot"; %ABVEULdot gives the best results (til now?)

[Hqp, Fqp, S, T, options] = MPCInitialization(nx, nu, dt, feedType);
MPC.Hqp = Hqp;
MPC.Fqp = Fqp;
MPC.S = S;
MPC.T = T;
MPC.nx = nx;
MPC.nu = nu;
MPC.options = options;

%Estados iniciais
states = [500 150 -200 23 0 0 0 0.05 -pi/2 0 0 0]';
psi = states(9);
theta = states(8);
phi = states(7);
R = [
        cos(psi) * cos(theta) -sin(psi) * cos(phi) + cos(psi) * sin(theta) * sin(phi) sin(psi) * sin(phi) + cos(psi) * sin(theta) * cos(phi);
        sin(psi) * cos(theta) cos(psi) * cos(phi) + sin(psi) * sin(theta) * sin(phi) -cos(psi) * sin(phi) + sin(psi) * sin(theta) * cos(phi);
        -sin(theta) cos(theta) * sin(phi) cos(theta) * cos(phi);
    ];
Wind = [2; -1; 0]*0; %WIND IN NED FRAME
wind = R'*Wind;
states(4:6) = states(4:6) - wind;

%Controles iniciais
controls = [0 0 0 0.5]';

%Carrega o Aerosonde
Aerosonde = AerosondeLoad();

%Trimming inicial
[states, controls, xdot] = trim(states, controls, 0, Aerosonde, Wind);
%%
switch feedType
   case "ABVEULdot"
        [X, U, E, Re, FInfo, Tout, t, LinSys, flag, EKF_OUT] = TrackingABVEULdot(states, controls, MPC,...
                                                            Aerosonde, tf, dt, curve_type, Wind);
   case "EABVEULdot"
        [X, U, E, Re, FInfo, Tout, t, LinSys, flag, EKF_OUT] = TrackingEABVEULdot(states, controls, MPC,...
                                                            Aerosonde, tf, dt, curve_type, Wind);
end

index = round((t-dt)/dt);
t = linspace(0, t, index);
%%
X = X(:, 1:index); %VANT
EKF_OUT = EKF_OUT(:, 1:index); %EKF
U = U(:, 1:index); %CONTROLES
E = E(:, 1:index); %ERROS MPC
Re = Re(:, 1:index); %REFERENCIA MPC
LinSys.noise = LinSys.noise(: ,1:index); %IGNORA

FInfo.F = FInfo.F(:, 1:index);
FInfo.state = FInfo.state(1:index); %INFORMAÇÕES DO CAMPO
%STATE = ESTADO DE TRANSIÇÃO
%FC = CAMPO DA CURVA
%FO = CAMPO DO OBSTACULO
%F = CAMPO TOTAL
%NEAROBS = OBSTACULO MAIS PROXIMO
%NEARP = PONTO DA CURA MAIS PROXIMO -> SO NO CAMPO DE DISTANCIA


%Vou plotar?
finalPlot = true;
%Vou animar?
finalAnimation = false;
%Vou salvar video?
finalVideo = false;

finalAnimation3D = false;

% Figs = plotAlphaInfo(X, U, E, EKF_OUT, FInfo, t, curve_type, finalPlot, finalAnimation, finalVideo);

%plotAnimation(X, U, E, EKF_OUT, FInfo, t, curve_type, finalAnimation3D);