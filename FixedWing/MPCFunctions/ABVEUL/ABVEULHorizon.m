function [horizon, stateOut, BetaOut, circOut, obstOut, pointOut, Fout, fcOut, foOut] =ABVEULHorizon(states, Vref, nx, dt,...
                                                                state, tbeta, circDir, curveType)
    horizon = zeros(15*nx, 1);
    
    [pn, pe, pd, u, v, w, phi, theta, psi, p, q, r] = deal(states{:});
    h = -pd;
    
    alpha = atan2(w, u);
    vt = norm([u; v; w]);
    beta = asin(v/vt);
    
    [Fout, ~, stateOut, BetaOut, circOut, obstOut, pointOut, fcOut, foOut] = BugField(curveType, [pe; pn; h], state, tbeta, circDir);  
   
    for i=1:nx
        pos = [pe pn h]';
        [F1, ~, state, tbeta, circDir] = BugField(curveType, pos, state, tbeta, circDir);      
        if norm(F1(1:2)) == 0
            [F1, ~, state, tbeta, circDir] = BugField(curveType, pos + [sin(psi); cos(psi); 0], state, tbeta, circDir);  
        end
        %I want VNE = Vref
        Fx = F1(1);
        Fy = F1(2);
        Fz = F1(3);
        
        %%
        Vd = -max(-3, min(3, Vref*Fz));
        %Vref^2 = Vne^2 + Vd^2
%         Vne = sqrt(Vref^2 - Vd^2);
        
        F_psi = atan2(Fx, Fy);
        F_psi = unwrap([psi, F_psi]);
        F_psi = F_psi(end);
          
        dpsi = tanh(F_psi - psi);
        
        gamma = asin(-Vd/Vref);
        g = dpsi*Vref/9.81;
        
        a = 1 - g*tan(alpha)*sin(beta);
        b = sin(gamma)/cos(beta);
        c = 1 + g^2*cos(beta)^2;
        
        tanphi = (g*cos(beta)*(a-b^2) + b*tan(alpha)*(c*(1 - b^2) + g^2*sin(beta)^2)^1/2)/(cos(alpha)*(a^2 - b^2*(1 + c*tan(alpha)^2)));
        F_phi = atan(tanphi);
        dphi = tanh(F_phi - phi);
        
        a = cos(alpha)*cos(beta);
        b = sin(F_phi)*sin(beta) + cos(F_phi)*sin(alpha)*cos(beta);
        F_theta = (a*b + sin(gamma)*sqrt(a^2 - sin(gamma)^2 + b^2))/(a^2 - sin(gamma)^2);
        F_theta = atan(F_theta);
        dtheta = tanh(F_theta - theta);
        
        psi = psi + dpsi*dt;
        theta = theta + dtheta*dt;
        phi = phi + dphi*dt;
        
        pe = pe + Fx*Vref*dt;
        pn = pn + Fy*Vref*dt;
        h = h - Vd*dt;
        
        horizon((i-1)*15+1:15*i, 1) = [0.05 0 Vref phi dphi theta dtheta psi dpsi 0 0 0 0 0 0]';
    end
end