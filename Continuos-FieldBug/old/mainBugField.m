%% Continuous Filedbug
% Need Adriano's matlab_lib added to path
% https://github.com/adrianomcr/matlab_lib

close all, clear, clc
%% Inputs

curve_type = 1; % 

p = [100 20 250]'; % Initial position
max_time = 150; %simulation time

MASTER_MODE = 1; % we will call auxiliar scripts
%% Selecting the curve 
fprintf('Program started\n')
[fC, O] = getParamCurve(curve_type);

dt = 10e-3;
t = 0:dt:max_time;
ds = pi/100;
s = 0:ds:2*pi;

C = fC(s,0);

%% Simulation

fprintf('Simulation started\n')
pause(1)

D_data = []; % for distance plot
f_data = []; % for |V| plot
field = [];
state_data = []; % for plot

vr = 23; 
d_in = 150;
Tbeta = 10; % recalculated below
circ_dir = 1; % recalculated below
state = 'follow curve'; % recalculated below
Beta = 0;

% O_points = {}; % Obstacle "detectable" points
% for k = 1:size(O,2)
%      Oi = O(:,k);
%      eQ = @(r) [Oi(4)*cos(r) + Oi(1); Oi(4)*sin(r) + Oi(2)];
%      sample = 50;
%      theta = linspace(0,2*pi, sample);
%      O_points(k) = {eQ(theta)};
% end

for k = 1:length(t)
    clc; fprintf('Simulating...\n%d%%\n',round(100*t(k)/t(end)))
    
    [f, D, state, Beta, circ_dir] = BugField(curve_type, p(:, k), state, Beta, circ_dir);
    
    switch state
        case 'follow curve'
            state_data(k) = 1;
        case 'transition to obstacle'
            state_data(k) = 2;
        case 'contour obstacle'
            state_data(k) = 3;
        case 'transition to curve'
            state_data(k) = 4;
    end
     
    %f = f/norm(f(1:2));
    %f(1:2) = f(1:2)*23;
    %f(3) = max(-3, min(3, 3*f(3)));
    
    p(:,k+1) = p(:,k) + f*dt;
    field = [field, f];
    f_data(k) = norm(f);
    D_data(k) = norm(D);
end

%% Static results
u = field(1, :);
v = field(2, :);
quiver(p(1, 1:end-1), p(2, 1:end-1), u, v)
return
%Trajectory
figure()
hold on
c = distinguishable_colors(1);
plot3(C(1,:),C(2,:),C(3,:),'k-','LineWidth',1)

aa = find(state_data == 1);
bb = find(state_data == 2 | state_data == 4);
cc = find(state_data == 3);
plot3(p(1,aa),p(2,aa),p(3,aa),'.','Color',[0 0 1])
plot3(p(1,bb),p(2,bb),p(3,bb),'.','Color',[0.7 0 0.7])
plot3(p(1,cc),p(2,cc),p(3,cc),'.','Color',[1 0 0])

plot3(p(1,end),p(2,end),p(3,end),'.','Color',[0 0 1],'LineWidth',1,'MarkerSize',25)

for k = 1:size(O,2)
    theta = linspace(0, 2*pi, 50);
    Oi = O(:,k);
    x = Oi(4)*cos(theta) + Oi(1); y = Oi(4)*sin(theta) + Oi(2);
    plot(x,y,'k--', 'LineWidth', 1.5);
end
hold off
grid on
axis equal
xlabel('$x$','interpreter','latex','FontSize',15)
ylabel('$y$','interpreter','latex','FontSize',15)
zlabel('$z$','interpreter','latex','FontSize',15)

%Distace
figure()
hold on
plot(t,D_data(:),'-','Color',c(1,:),'LineWidth',2)
grid on
xlabel('$t$','interpreter','latex','FontSize',15)
ylabel('$D$','interpreter','latex','FontSize',15)

%Velocity
figure()
hold on
plot(t,f_data(:),'.','Color',c(1,:))
grid on
xlabel('$t$','interpreter','latex','FontSize',15)
ylabel('$\|V\|$','interpreter','latex','FontSize',15)