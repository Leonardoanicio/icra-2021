t = linspace(0, 200, length(X));

subplot(2, 1, 1, 'Units', 'normalized', 'Position', [0.05, 0.55, 0.9, 0.375]);
plot(t, U(1:3, :)', 'LineWidth', 2);
ylabel('Deflection (rad)', 'FontSize', 15);
legend("Aileron", 'Elevator', 'Rudder', 'FontSize', 15);
grid;
xticklabels([]);

subplot(2, 1, 2, 'Units', 'normalized', 'Position', [0.05, 0.1, 0.9, 0.375]);
plot(t, U(4, :)*100, 'LineWidth', 2);
legend('Throttle', 'FontSize', 15);
ylabel('Percentage (%)', 'FontSize', 15);
xlabel('Time (s)')
sgtitle("Control Signals");
grid;
set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');