%% Continuous Filedbug
% Need Adriano's matlab_lib added to path
% https://github.com/adrianomcr/matlab_lib

close all, clear, clc
%% Inputs

curve_type = 11; % 1-circle 2-race track  4-smooth square 5-crown 6-vertical circle 
%9-time varying circle 11 - Lemniscata (VANT)
%**3-perfect square **7-crazy loops **8-polar rose 
c_x = 0; c_y = 0; c_z = 0; %curve centers
r_x = 5; r_y = 5; r_z = 1; %curve alongations

p = [-300 200 2]'; % Initial position
max_time = 100; %simulation time

ws = [-1 1 -1 1 -1 1]*700; % animation plot size
r_tilde = 15; % robot radius (just for plot)
animation_speed = 10; % just for plot

MASTER_MODE = 1; % we will call auxiliar scripts
%% Selecting the curve 
fprintf('Program started\n')
curve %auxiliar script that can be run separately

dt = 10e-3;
t = 0:dt:max_time;
ds = pi/100;
s = 0:ds:2*pi;

C = fC(s,0);

%% Simulation

fprintf('Simulation started\n')
pause(1)

D_data = []; % for distance plot
f_data = []; % for |V| plot
field = [];
state_data = []; % for plot

vr = 23; 
d_in = 150;
Tbeta = 10; % recalculated below
circ_dir = 1; % recalculated below
state = 'follow curve'; % recalculated below
Beta = 0;

for k = 1:length(t)
    clc; fprintf('Simulating...\n%d%%\n',round(100*t(k)/t(end)))
    
    [f, D, state, Beta, circ_dir] = BugFieldAlpha(O, p(:, k), state, Beta, circ_dir);
    
    switch state
        case 'follow curve'
            state_data(k) = 1;
        case 'transition to obstacle'
            state_data(k) = 2;
        case 'contour obstacle'
            state_data(k) = 3;
        case 'transition to curve'
            state_data(k) = 4;
    end
    
    p(:,k+1) = p(:,k) + f*dt;
    field = [field, f];
    f_data(k) = norm(f);
    D_data(k) = norm(D);
end

%% Static results
%Quiver
u = field(1, :);
v = field(2, :);
w = field(3, :);
quiver3(p(1, 1:end-1), p(2, 1:end-1), p(3, 1:end-1), u, v, w);
%Trajectory
figure()
hold on
c = distinguishable_colors(1);

aa = find(state_data == 1);
bb = find(state_data == 2 | state_data == 4);
cc = find(state_data == 3);
plot3(p(1,aa),p(2,aa),p(3,aa),'.','Color',[0 0 1])
plot3(p(1,bb),p(2,bb),p(3,bb),'.','Color',[0.7 0 0.7])
plot3(p(1,cc),p(2,cc),p(3,cc),'.','Color',[1 0 0])

plot3(p(1,end),p(2,end),p(3,end),'.','Color',[0 0 1],'LineWidth',1,'MarkerSize',25)

return

for k = 1:size(O,2)
    theta = linspace(0, 2*pi, 50);
    Oi = O(:,k);
    x = Oi(4)*cos(theta) + Oi(1); y = Oi(4)*sin(theta) + Oi(2);
    plot(x,y,'k--', 'LineWidth', 1.5);
end
hold off
grid on
axis equal
xlabel('$x$','interpreter','latex','FontSize',15)
ylabel('$y$','interpreter','latex','FontSize',15)
zlabel('$z$','interpreter','latex','FontSize',15)

%Distace
figure()
hold on
plot(t,D_data(:),'-','Color',c(1,:),'LineWidth',2)
grid on
xlabel('$t$','interpreter','latex','FontSize',15)
ylabel('$D$','interpreter','latex','FontSize',15)

%Velocity
figure()
hold on
plot(t,f_data(:),'.','Color',c(1,:))
grid on
xlabel('$t$','interpreter','latex','FontSize',15)
ylabel('$\|V\|$','interpreter','latex','FontSize',15)