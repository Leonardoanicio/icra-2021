function [f, D, state, Beta, circ_dir, near_obst, fc, fo] = BugFieldAlpha(p, curveType, state, Beta, circ_dir, time)
    d_in = 150;
    vr = 23;
    dt = 0.01;
        
    fo = zeros(3, 1);
    
    %Pega a curva
    [f_a1, f_a2, P, a_curve, b_curve, c_curve, O, O_var, kp] = getCurve(curveType);
   
    % find the nearest obstacle and the vector r
    near_obst = 1;
    r = [inf; inf];
    for j = 1:size(O,2)
        Oj = [O(1, j) + O_var{1,j}(time); O(2, j) + O_var{2,j}(time)];
        new_r = Oj - p(1:2);
        new_r = new_r - O(4,j)*new_r/norm(new_r);
        if norm(r) > norm( new_r ) 
           r = new_r;
           near_obst = j;
        end
    end
    
    D = norm(r);
    
    if size(O, 2) > 0
        Tbeta = 0.5*(d_in - O(4, near_obst))/vr;
    else
        Tbeta = inf;
    end
    r = [r;0];
    
    %Esse tem que calcular sempre, então fica aqui
    [fc, ~] = curve_vector_alpha2(p, a_curve, b_curve, c_curve, f_a1, f_a2, P, kp, vr);
    
    switch state
        case 'follow curve'
            f = fc;
            if norm(r) < d_in && r'*fc > 0
                state = 'transition to obstacle';
                Beta = 0;
                circ_dir = cross([r(1:2);0],[fc(1:2);0]);
                circ_dir = -sign(circ_dir(3));
            end
        case 'transition to obstacle'
            fo = obstacle_vector_alpha(p, O(:,near_obst), O_var{1,near_obst}, O_var{2,near_obst}, r, circ_dir, time, vr);
            f = (1 + 2*Beta^3 - 3*Beta^2)*fc + (-2*Beta^3 +3 *Beta^2)*fo;
            f(3) = fc(3); % keep following z component
                       
            Beta = Beta + dt/Tbeta;
            if Beta > 1
                state = 'contour obstacle';
            end
        case 'contour obstacle'
            fo = obstacle_vector_alpha(p, O(:,near_obst), O_var{1,near_obst}, O_var{2,near_obst}, r, circ_dir, time, vr);
            f = fo;
            f(3) = fc(3); % keep following z component

            if r'*fc < 0
                state = 'transition to curve';
                Beta = 0;
            end

        case 'transition to curve'
            fo = obstacle_vector_alpha(p, O(:,near_obst), O_var{1,near_obst}, O_var{2,near_obst}, r, circ_dir, time, vr);
            %f = sin(Beta*pi/2)^2*fc + cos(Beta*pi/2)^2*fo;
            f = (-2*Beta^3 +3 *Beta^2)*fc + (1 + 2*Beta^3 - 3*Beta^2)*fo;
            f(3) = fc(3); % keep following z component
            
            Beta = Beta + dt/Tbeta;
            if Beta > 1
                state = 'follow curve';
            end
            
            %Se aparecer um obstaculo no caminho, volta a circular
            if norm(r) < d_in && r'*fc > 0
                state = 'transition to obstacle';
                Beta = 0;
                circ_dir = cross([r(1:2);0],[fc(1:2);0]);
                circ_dir = -sign(circ_dir(3));
            end
            
        otherwise
            state = 'follow curve';  
    end
end

