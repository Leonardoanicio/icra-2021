close all;

dec_rate = 5;

P_North = pn(1:dec_rate:end);
P_East = pe(1:dec_rate:end);
h = -pd(1:dec_rate:end);

% Print the trajectory of the UAV
figure('units','normalized','outerposition',[0 0 1 1])
p2 = plot3(P_East(1)/1e3,P_North(1)/1e3,h(1)/1e3,'-','Color',[1 0 0],'LineWidth',1);
title('Case I');
hold on;

C = GetCurveData();
px = C(1, :);
py = C(2, :);
pz = C(3, :);
% p1 = plot3(C(1, :)/1e3, C(2, :)/1e3, cz(1)*ones(size(C(3,:)))/1e3,'k');
plot3(P_East(1)/1e3,P_North(1)/1e3,h(1)/1e3,'o','Color',[1 0 0],'LineWidth',2,'MarkerSize',12);

hold off;
xlabel('East Position (km)');
ylabel('North Position (km)');
zlabel('Height (km)');
grid on;
% title('Trajectory');
axis equal;


p = [P_East', P_North', h']/1e3;
p = p';

pitch = theta(1:dec_rate:end)';
yaw = psi(1:dec_rate:end)';
roll = phi(1:dec_rate:end)';

% CZ = cz(1:dec_rate:end);
axis equal
w_s = [min(P_East) max(P_East) min(P_North) max(P_North) min(h) max(h)]/1e3 + [-1 1 -1 1 -1 1]*0.1;
axis(w_s);


% ANIMATION
% ----------  ----------  ----------  ----------  ----------  ----------  ----------
% figure(1)
hold on
h1 = fill3(0,0,0,'b','LineWidth',2); h1.LineStyle = 'none';
h2 = fill3(0,0,0,'b','LineWidth',2); h2.LineStyle = 'none';
h3 = fill3(0,0,0,'b','LineWidth',2); h3.LineStyle = 'none';
h4 = fill3(0,0,0,'b','LineWidth',2); h4.LineStyle = 'none';
h5 = fill3(0,0,0,'b','LineWidth',2); h5.LineStyle = 'none';
h6 = fill3(0,0,0,'b','LineWidth',2); h6.LineStyle = 'none';

h7 = fill3(0,0,0,'b','LineWidth',2); h7.LineStyle = 'none';
h8 = fill3(0,0,0,'b','LineWidth',2); h8.LineStyle = 'none';
h9 = fill3(0,0,0,'b','LineWidth',2); h9.LineStyle = 'none';
grid on
hold off
uav_e = 0.07;
uav_size = 20; %uav_size = uav_size^1;
UAV0 = [[0 0.5 0 0.2   0 0.5 0 0.2];[1 0 -1 0   1 0 -1 0]; [1 1 1 1   -1 -1 -1 -1]*uav_e]/uav_size;
TAIL0 = [[0.2 -0.7 -0.7 -0.55 -0.4 0.2     0.2 -0.7 -0.7 -0.55 -0.4 0.2];[1 1 1 1 1 1     -1 -1 -1 -1 -1 -1]*uav_e;[-1 -1 3 3 1 1     -1 -1 3 3 1 1]*uav_e]/uav_size;
k = 1;
R = [cos(yaw(k)) sin(yaw(k)) 0; -sin(yaw(k)) cos(yaw(k)) 0; 0 0 1];
Rroll = [1 0 0; 0 cos(roll(k)) sin(roll(k)); 0 -sin(roll(k)) cos(roll(k))];
Rpitch = [cos(pitch(k)) 0 -sin(pitch(k)); 0 1 0; sin(pitch(k)) 0 cos(pitch(k))];
UAV = R*Rpitch*Rroll*UAV0;
TAIL = R*Rpitch*Rroll*TAIL0;
set(h1,'XData',UAV(1,1:4)+p(1,k),'YData',UAV(2,1:4)+p(2,k),'ZData',UAV(3,1:4)+p(3,k));
set(h2,'XData',UAV(1,5:8)+p(1,k),'YData',UAV(2,5:8)+p(2,k),'ZData',UAV(3,5:8)+p(3,k));
set(h3,'XData',UAV(1,[1 5 6 2])+p(1,k),'YData',UAV(2,[1 5 6 2])+p(2,k),'ZData',UAV(3,[1 5 6 2])+p(3,k));
set(h4,'XData',UAV(1,[2 6 7 3])+p(1,k),'YData',UAV(2,[2 6 7 3])+p(2,k),'ZData',UAV(3,[2 6 7 3])+p(3,k));
set(h5,'XData',UAV(1,[3 7 8 4])+p(1,k),'YData',UAV(2,[3 7 8 4])+p(2,k),'ZData',UAV(3,[3 7 8 4])+p(3,k));
set(h6,'XData',UAV(1,[4 8 5 1])+p(1,k),'YData',UAV(2,[4 8 5 1])+p(2,k),'ZData',UAV(3,[4 8 5 1])+p(3,k));
set(h6,'XData',UAV(1,[4 8 5 1])+p(1,k),'YData',UAV(2,[4 8 5 1])+p(2,k),'ZData',UAV(3,[4 8 5 1])+p(3,k));
set(h7,'XData',TAIL(1,1:6)+p(1,k),'YData',TAIL(2,1:6)+p(2,k),'ZData',TAIL(3,1:6)+p(3,k));
set(h8,'XData',TAIL(1,7:12)+p(1,k),'YData',TAIL(2,7:12)+p(2,k),'ZData',TAIL(3,7:12)+p(3,k));
set(h9,'XData',TAIL(1,[1 2 8 7])+p(1,k),'YData',TAIL(2,[1 2 8 7])+p(2,k),'ZData',TAIL(3,[1 2 8 7])+p(3,k));
pause(1)
set(1,'Position',[0.3542 0.0324 0.6354 0.6806])
v = VideoWriter('animation.avi','Motion JPEG AVI');
open(v);
for k = 1:2:round(length(p(1,:))/1)
%      frame = getframe(gcf);
%      writeVideo(v,frame);
    R = [cos(yaw(k)) sin(yaw(k)) 0; -sin(yaw(k)) cos(yaw(k)) 0; 0 0 1]';
    Rroll = [1 0 0; 0 cos(roll(k)) sin(roll(k)); 0 -sin(roll(k)) cos(roll(k))]';
    Rpitch = [cos(pitch(k)) 0 -sin(pitch(k)); 0 1 0; sin(pitch(k)) 0 cos(pitch(k))]';
    UAV = R*Rpitch*Rroll*UAV0;
    TAIL = R*Rpitch*Rroll*TAIL0;


    set(h1,'XData',UAV(1,1:4)+p(1,k),'YData',UAV(2,1:4)+p(2,k),'ZData',UAV(3,1:4)+p(3,k));
    set(h2,'XData',UAV(1,5:8)+p(1,k),'YData',UAV(2,5:8)+p(2,k),'ZData',UAV(3,5:8)+p(3,k));
    set(h3,'XData',UAV(1,[1 5 6 2])+p(1,k),'YData',UAV(2,[1 5 6 2])+p(2,k),'ZData',UAV(3,[1 5 6 2])+p(3,k));
    set(h4,'XData',UAV(1,[2 6 7 3])+p(1,k),'YData',UAV(2,[2 6 7 3])+p(2,k),'ZData',UAV(3,[2 6 7 3])+p(3,k));
    set(h5,'XData',UAV(1,[3 7 8 4])+p(1,k),'YData',UAV(2,[3 7 8 4])+p(2,k),'ZData',UAV(3,[3 7 8 4])+p(3,k));
    set(h6,'XData',UAV(1,[4 8 5 1])+p(1,k),'YData',UAV(2,[4 8 5 1])+p(2,k),'ZData',UAV(3,[4 8 5 1])+p(3,k));
    set(h6,'XData',UAV(1,[4 8 5 1])+p(1,k),'YData',UAV(2,[4 8 5 1])+p(2,k),'ZData',UAV(3,[4 8 5 1])+p(3,k));
    set(h7,'XData',TAIL(1,1:6)+p(1,k),'YData',TAIL(2,1:6)+p(2,k),'ZData',TAIL(3,1:6)+p(3,k));
    set(h8,'XData',TAIL(1,7:12)+p(1,k),'YData',TAIL(2,7:12)+p(2,k),'ZData',TAIL(3,7:12)+p(3,k));
    set(h9,'XData',TAIL(1,[1 2 8 7])+p(1,k),'YData',TAIL(2,[1 2 8 7])+p(2,k),'ZData',TAIL(3,[1 2 8 7])+p(3,k));

    set(p2,'XData', P_East(1:k)/1e3, 'YData', P_North(1:k)/1e3, 'ZData', h(1:k)/1e3)
    set(1,'Position',[0.3542 0.0324 0.6354 0.6806])
    drawnow
    pause(0.1)
end
% ----------  ----------  ----------  ----------  ----------  ----------  ----------
close(v);
