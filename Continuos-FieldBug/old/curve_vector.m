function [f, D, near_point] = curve_vector(p, fC, t, kp)
    %% Other variables
%     kp = 0.0075;
    sample_size = 500;
    ds = 2*pi/sample_size;
    
    %% D_i
    %Round 1
    persistent s
    if isempty(s)
        s = linspace(0,2*pi,sample_size);
    end
    C = fC(s,t);  
    D = vecnorm(C - p);
    i_close = find(D == min(D), 1);
    
    %Round 2
    s = linspace(s(i_close)-ds, s(i_close)+ds, 50);
    C = fC(s,t);   
    D = vecnorm(C - p);
    i_close = find(D == min(D), 1);
    ds = ds/50;
    
    %Round 3
    s = linspace(s(i_close)-ds, s(i_close)+ds, 50);
    C = fC(s,t);   
    D = vecnorm(C - p);
    i_close = find(D == min(D), 1);
    s_opt = s(i_close);
    
    C_opt = fC(s_opt, t);
    near_point = C_opt;
    D = p-C_opt;
    D_i_unit = D/(norm(D)+1e-6);
    
    %% Scaling functions
%     G = -(2/pi)*atan(kp*norm(D));
    G = -tanh(norm(D)*kp);
    H = sqrt(1-G^2);
    
    %% Vectors for robot i
    %Tangent vector for robot i
    T = (fC(s_opt+ds,t)-fC(s_opt-ds,t));
    T = T/norm(T);
    
%     %% Feedback foward
%     
%     TT = eye(3) - T*T';
%     
%     C_prox = fC(s,t+ds); 
%     
%     D_i_prox = inf;
%     for k = 1:1:length(s)
%         if(norm(p - C_prox(:,k)) < norm(D_i_prox))
%             D_i_prox = p - C_prox(:,k);
%             i_close = k;
%         end
%     end
%     [s_opt] = fminbnd(@(s) f_cost(p,s), s(i_close)-pi/50, s(i_close)+pi/50);
%     C_opt = fC(s_opt,t+ds);
%     D_i_prox = p-C_opt;
%     
%     C_prev = fC(s,t-ds); 
%     
%     D_i_prev = inf;
%     for k = 1:1:length(s)
%         if(norm(p - C_prev(:,k)) < norm(D_i_prev))
%             D_i_prev = p - C_prev(:,k);
%             i_close = k;
%         end
%     end
%     [s_opt] = fminbnd(@(s) f_cost(p,s), s(i_close)-pi/50, s(i_close)+pi/50);
%     C_opt = fC(s_opt,t-ds);
%     D_i_prev = p-C_opt;
%     
%     dDdt = (D_i_prox - D_i_prev)/(2*ds);
    
    %% Vector field for robot i
    f_D = G*D_i_unit; f_T = H*T; %f_ff = - TT*dDdt;
    vr = 1;
    %scale_factor = -(f_D+f_T)'*f_ff + sqrt( ((f_D+f_T)'*f_ff)^2 + vr^2 - norm(f_ff)^2 );
    
    f = f_D + f_T;
    %f = scale_factor*(f_D + f_T) + f_ff;
end