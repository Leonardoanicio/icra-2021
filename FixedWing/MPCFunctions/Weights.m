function [A, B, Q, R, P] = Weights(feedType, dt)
    
    switch feedType
        case "ABVEULdot"
            A = eye(6);
            B = eye(6)*dt;
            Q = diag([1 1 0.1 5 5 5]);
            R = diag([1 1 1 1 1 1]);
        case "EABVEULdot"
            A = eye(6);
            B = eye(6)*dt;
            Q = diag([1 1 0.1 5 5 5]);
            R = diag([1 1 1 1 1 1]);
    end
 
    Q = blkdiag(Q, zeros(size(R)));
    A = [A B; zeros(size(B')) eye(size(B, 2))];
    B = [B; eye(size(B, 2))];
    
    P = idare(A,B,Q,R); 
%     P = Q;
end