function [Aeq, Beq, Aineq, Bineq] = VEULdotBounds(x, T, S, Coeffs_P, Coeffs_M, Beta, controls, ...
                                        nx, nu, UpperBounds, LowerBounds)   

    AEQ = (eye(6) - Coeffs_M*Coeffs_P)*Beta;
    %Aeq = kron(tril(ones(nu, nu)), AEQ);
    Aeq = kron(eye(nu), AEQ);
    Beq = zeros(6*nu, 1);
    
    AINEQ1 = Coeffs_P*Beta;
    AINEQ1 = kron(tril(ones(nu, nu)), AINEQ1);
    
    BINEQ1U = repmat(UpperBounds - controls, nu, 1);
    BINEQ1L = repmat(LowerBounds - controls, nu, 1);
    
    AINEQ2 = kron(eye(nu), Coeffs_P*Beta);
    
    BINEQ2U = 0.01*ones(4*nu, 1);
    BINEQ2L = -0.01*ones(4*nu, 1);
    
    K = zeros(1, 8);
    K(1, 1) = 1;
    K = kron(eye(nx), K);
    
    AINEQ3 = K*S;
    
    BINEQ3U = repmat([30], nx, 1);
    BINEQ3U = BINEQ3U - K*T*x;
    BINEQ3L = repmat([18], nx, 1);
    BINEQ3L = BINEQ3L - K*T*x;
    
    Aineq = [AINEQ1; -AINEQ1; AINEQ2; -AINEQ2; AINEQ3; -AINEQ3];
    Bineq = [BINEQ1U; -BINEQ1L; BINEQ2U; -BINEQ2L; BINEQ3U; -BINEQ3L];   
    
    %Região terminal dphi = 0, dtheta = 0, dvt = 0;
    %Como ddtheta e ddphi são dadas diretamente por dv -> x0 + sum(dv) = 0
    %dphi = 0, dtheta = 0 -> angulos constantes
    %dbeta = 0, dalpha = 0, dvt = 0 -> velocidade constante
    K = zeros(2, 4*nu);
    K(1, 2:4:end) = 1;
    K(2, 3:4:end) = 1;
    AEQ2 = K;
    BEQ2 = -x(2:3);

    %x+ = A*x + B*dv
    %x+ - x = A*x - x + B*dv = (A - I)*x + B*dv (para este caso) ->
    %(A - I) = 0 -> x+ - x = B*dv -> dv = 0
    AEQ3 = [zeros(4, 4*(nu - 1)), eye(4)];
    BEQ3 = zeros(4, 1);
% 
%     Aeq = [Aeq; AEQ2; AEQ3];
%     Beq = [Beq; BEQ2; BEQ3];
end