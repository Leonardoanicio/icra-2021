function [f, E] = curve_vector_alpha(p, a, b, c)
    kp = 500;
    
    p(1) = p(1)/a; p(2) = p(2)/b; p(3) = p(3)/c;
    a = 1; b = 1; c = 1;
    x = p(1); y = p(2); z = p(3);
    E = (y - (2*b*x*z)/(a*c))^2/2 + (x^2/a^2 + z^2/c^2 - 1)^2/2;
    
    G = -2/pi * atan(kp*E);
    H = sqrt(1 - G^2);
    
    gradE = [(2*x*(x^2/a^2 + z^2/c^2 - 1))/a^2 - (2*b*z*(y - (2*b*x*z)/(a*c)))/(a*c);...
                                                   y - (2*b*x*z)/(a*c);...
            (2*z*(x^2/a^2 + z^2/c^2 - 1))/c^2 - (2*b*x*(y - (2*b*x*z)/(a*c)))/(a*c)];
        
    g1_vec_g2 = [-(2*z)/c^2;...
       (4*b*x^2)/(a^3*c) - (4*b*z^2)/(a*c^3);...
                             (2*x)/a^2];
                         
    f = G*gradE/(norm(gradE)+1e-6) + H*g1_vec_g2/(norm(g1_vec_g2) + 1e-6);
    %f = f/(norm(f)+1e-6);
end