function [f, D] = curve_vector(p, fC, t)
    %% Other variables
    kp = 0.010;
    ds = 1e-6;
    f_cost = @(p,s) (p-fC(s,t))'*(p-fC(s,t));
    sample_size = 500;
    s = linspace(0,2*pi,sample_size);
    C = fC(s,t);   
    
    %% D_i
    
    D = vecnorm(C - p);
    i_close = find(D == min(D), 1);
    
    %Refine the distace
    [s_opt] = fminbnd(@(s) f_cost(p,s), s(i_close)-pi/50, s(i_close)+pi/50);
    C_opt = fC(s_opt,t);
    D = p-C_opt;
    D_i_unit = D/(norm(D)+1e-6);
    
    %% Scaling functions
    G = -(2/pi)*atan(kp*norm(D));
    H = sqrt(1-G^2);
    
    %% Vectors for robot i
    %Tangent vector for robot i
    T = (fC(s_opt+ds,t)-fC(s_opt-ds,t));
    T = T/norm(T);
    
%     %% Feedback foward
%     
%     TT = eye(3) - T*T';
%     
%     C_prox = fC(s,t+ds); 
%     
%     D_i_prox = inf;
%     for k = 1:1:length(s)
%         if(norm(p - C_prox(:,k)) < norm(D_i_prox))
%             D_i_prox = p - C_prox(:,k);
%             i_close = k;
%         end
%     end
%     [s_opt] = fminbnd(@(s) f_cost(p,s), s(i_close)-pi/50, s(i_close)+pi/50);
%     C_opt = fC(s_opt,t+ds);
%     D_i_prox = p-C_opt;
%     
%     C_prev = fC(s,t-ds); 
%     
%     D_i_prev = inf;
%     for k = 1:1:length(s)
%         if(norm(p - C_prev(:,k)) < norm(D_i_prev))
%             D_i_prev = p - C_prev(:,k);
%             i_close = k;
%         end
%     end
%     [s_opt] = fminbnd(@(s) f_cost(p,s), s(i_close)-pi/50, s(i_close)+pi/50);
%     C_opt = fC(s_opt,t-ds);
%     D_i_prev = p-C_opt;
%     
%     dDdt = (D_i_prox - D_i_prev)/(2*ds);
    
    %% Vector field for robot i
    f_D = G*D_i_unit; f_T = H*T; %f_ff = - TT*dDdt;
    vr = 1;
    %scale_factor = -(f_D+f_T)'*f_ff + sqrt( ((f_D+f_T)'*f_ff)^2 + vr^2 - norm(f_ff)^2 );
    
    f = f_D + f_T;
    %f = scale_factor*(f_D + f_T) + f_ff;
end