function [X] = plotAlpha(alpha1, alpha2, P)
    %Procura por x0
    F = @(x) [
            alpha1(x(1), x(2), x(3));
            alpha2(x(1), x(2), x(3));
            ];
    options = optimoptions('fsolve', 'Display', 'none', 'FunctionTolerance', 1e-6,...
                            'MaxFunctionEvaluations', 1e3);
    x0 = fsolve(F, ones(3, 1), options);
    
    %Refina x0
    for i=1:15000
        dx = AlphaField(alpha1, alpha2, P, x0);
        dx = dx/norm(dx);
        x0 = x0 + dx*0.1;
        if abs(alpha1(x0(1), x0(2), x0(3))) < 0.1 && abs(alpha2(x0(1), x0(2), x0(3))) < 0.1
            break
        end
    end
        
    X = [x0];
    
    %Continua seguindo o campo
    for i=1:15000
        dx = AlphaField(alpha1, alpha2, P, x0);
        dx = dx/norm(dx);
        
        if norm(x0 + dx - X(:, 1)) < norm(x0 - X(:, 1)) && norm(x0 - X(:, 1)) < 2
            x0 = x0 + dx;
            X = [X, x0];
            break
        end
        x0 = x0 + dx;
        X = [X, x0];
    end
    plot3(X(1, :), X(2, :), X(3, :), 'cyan', 'LineWidth', 1);
    xlabel('X');
    ylabel('Y');
    zlabel('Z');
    
    axis equal
end