function plotAnimation(X, U, E, EKF, FInfo, t, curveType, finalVideo)
    %Pega a curva
    [f_a1, f_a2, P, a_curve, b_curve, c_curve, O, O_var, kp] = getCurve(curveType);

    pn = X(1,:);
    pe = X(2,:);
    pd = X(3,:);
    ur = X(4,:);
    vr = X(5,:);
    wr = X(6,:);
    phi = X(7,:);
    theta = X(8,:);
    psi = X(9,:);
    p = X(10,:);
    q = X(11,:);
    r = X(12, :);
    
    vt = vecnorm([ur; vr; wr]);


    %Plota a curva com os dados
    fTraj = figure('units','normalized','outerposition',[0 0 1 1]);
    hold on; 
    rc = plot_alpha(curveType);
    aa = find(FInfo.state == 1);
    bb = find(FInfo.state == 2 | FInfo.state == 4);
    cc = find(FInfo.state == 3);
    plot3(pe(aa), pn(aa), -pd(aa), '.', 'Color', [0 0 1]);
    pa = plot3(0,0,0, 'Color', [0 0 1]);
    plot3(pe(bb), pn(bb), -pd(bb), '.', 'Color', [0.7 0 0.7]);
    pb = plot3(0,0,0, 'Color', [0.7 0 0.7]);
    plot3(pe(cc), pn(cc), -pd(cc), '.', 'Color', [1 0 0]);
    pc = plot3(0,0,0, 'Color', [1 0 0]);

    zmin = min(-pd);
    angle = linspace(0, 2*pi, 50);
    zz = zmin*ones(size(angle));

    %Plota os obstaculos
    for k = 1:size(O, 2)
        Oi = O(:,k);
        x = Oi(1);
        y = Oi(2);
        r = Oi(4);

        %Plota o obstáculo
        xx = x + r*cos(angle);
        yy = y + r*sin(angle);
        plot3(xx, yy, zz, 'Color', [0 0 0]);

        %Curva de referencia
        xx = x + (r + 50)*cos(angle);
        yy = y + (r + 50)*sin(angle);
        plot3(xx, yy, zz, 'Color', [1 1 0]);

        %Din
        xx = x + (r + 150)*cos(angle);
        yy = y + (r + 150)*sin(angle);
        plot3(xx, yy, zz, 'Color', [0 1 0]);
    end
    %zlim([min(-pd) max(-pd)]);
    
    fake_legend_curve = plot3(1e6, 0, 0, 'k-');
    
    axis equal; axis([min(pe)-100 max(pe)+100 min(pn)-100 max(pn)+100 min(-pd)-100 max(-pd)+100]);
    view(2)
    legend([pa, pb, pc, fake_legend_curve], 'Follow Curve', 'Transition', 'Contour Obstacle', 'Curve');
    drawnow;
%%
    fAnimation = figure('units','normalized','outerposition',[0 0 1 1]);
    figure(fAnimation); hold on; grid on; view(3); axis equal; axis([min(pe)-50 max(pe)+50 min(pn)-50 max(pn)+50 min(-pd)-50 max(-pd)+50]);
    rc = plot_alpha(curveType);
    fake_legend_curve = plot3(1e6, 0, 0, 'k-');
    fake_legend_din = plot3(1e6, 0, 0, 'g--');
        

    drawnow;
    POB = [];
    POB_ref = [];
    POB_Din = [];
    angle = linspace(0, 2*pi, 50);
    zz = zmin*ones(size(angle));
    for k = 1:size(O, 2)
        Oi = O(:,k);
        x = Oi(1);
        y = Oi(2);
        r = Oi(4);
        h = 400;

        POB = [POB plotCylinder(x,y,h,r, [0.1 0.1 0.1], 0.9)];

        POB_ref = [POB_ref plotCylinder(x,y,h,r + 50, [1 1 0], 0.2)];

        POB_Din = [POB_Din plotCylinder(x,y,h,r + 150, [0 1 0], 0.1)];

%                 %Plota o obstáculo
%                 xx = x + r*cos(angle);
%                 yy = y + r*sin(angle);
%                 POB = [POB plot3(xx, yy, zz, '--', 'Color', [0 0 0])];
% 
%                 %Curva de referencia
%                 xx = x + (r + 50)*cos(angle);
%                 yy = y + (r + 50)*sin(angle);
%                 POB_ref = [POB_ref plot3(xx, yy, zz, '--', 'Color', [1 1 0])];
% 
%                 %Din
%                 xx = x + (r + 150)*cos(angle);
%                 yy = y + (r + 150)*sin(angle);
%                 POB_Din = [POB_Din plot3(xx, yy, zz, '--', 'Color', [0 1 0])];
    end

    %hold on;
    if finalVideo
        file_name = ['3D_curve_', num2str(curveType), '.avi'];
        V = VideoWriter(file_name);
        V.Quality = 75;
        V.FrameRate = 25;
        open(V);
    end
    p1 = plot3(pe(1), pn(1), -pd(1), 'ko', 'MarkerSize', 10, 'MarkerFaceColor', 'blue'); 
    traj_animation = plot3(pe(1), pn(1), -pd(1), 'k--'); 
    %p2 = plot3(pe(1), pn(1), -pd(1), 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'red'); 
    f1 = quiver3(pe(1), pn(1), -pd(1), 0, 0, 0, 'green', 'LineWidth', 2, 'AutoScale', 'off', 'MaxHeadSize', 2);
    f2 = quiver3(pe(1), pn(1), -pd(1), 0, 0, 0, 'blue', 'LineWidth', 2, 'AutoScale', 'off', 'MaxHeadSize', 2);
    f3 = quiver3(pe(1), pn(1), -pd(1), 0, 0, 0, 'red', 'LineWidth', 2, 'AutoScale', 'off', 'MaxHeadSize', 2);

    %axis equal
    pause()
    for t=1:30:length(pe)
        if FInfo.state(t) == 1
            p1.MarkerFaceColor = [0 0 1];
        elseif FInfo.state(t) == 3
            p1.MarkerFaceColor = [1 0 0];
        else
            p1.MarkerFaceColor = [0.7 0 0.7];
        end

        for k = 1:size(O, 2)
            Oi = O(:,k);
            Oix_var = O_var{1,k};
            Oiy_var = O_var{2,k};
            x = Oi(1) + Oix_var(t*0.01);
            y = Oi(2) + Oiy_var(t*0.01);
            h = 400;
            r = Oi(4);
            setCylinder(POB(k), x, y, h, r)
            setCylinder(POB_ref(k), x, y, h, r+50)
            setCylinder(POB_Din(k), x, y, h, r+150)
%                     xx = x + r*cos(angle);
%                     yy = y + r*sin(angle);
%                     POB(k).XData = xx;
%                     POB(k).YData = yy;
%                     
%                     xx = x + (r + 50)*cos(angle);
%                     yy = y + (r + 50)*sin(angle);
%                     POB_ref(k).XData = xx;
%                     POB_ref(k).YData = yy;
%                     
%                     xx = x + (r + 150)*cos(angle);
%                     yy = y + (r + 150)*sin(angle);
%                     POB_Din(k).XData = xx;
%                     POB_Din(k).YData = yy;
        end

        f = FInfo.F(:, t);
        fc = FInfo.Fc(:, t);
        fo = FInfo.Fo(:, t);

        f = 50*f/(1e-3 + norm(f));
        fc = 50*fc/(1e-3 + norm(fc));
        fo = 50*fo/(1e-3 + norm(fo));

        p1.XData = pe(t);
        p1.YData = pn(t);
        p1.ZData = -pd(t);
        
        traj_animation.XData = pe(1:t);
        traj_animation.YData = pn(1:t);
        traj_animation.ZData = -pd(1:t);

        %p2.XData = FInfo.nearp(1, t);
        %p2.YData = FInfo.nearp(2, t);
        %p2.ZData = FInfo.nearp(3, t);

        f1.XData = pe(t); f1.YData = pn(t); f1.ZData = -pd(t);
        f2.XData = pe(t); f2.YData = pn(t); f2.ZData = -pd(t);
        f3.XData = pe(t); f3.YData = pn(t); f3.ZData = -pd(t);

        f1.UData = f(1); f1.VData = f(2); f1.WData = 1;
        f2.UData = fc(1); f2.VData = fc(2); f2.WData = 1;
        f3.UData = fo(1); f3.VData = fo(2); f3.WData = 1;
        
        legend([p1, traj_animation, f1, f2, f3, POB(1), fake_legend_curve, fake_legend_din],'UAV', 'Trajectory',...
            'Total Field', 'Curve Field', 'Obstacle Field',...
             'Obstacles', 'Curve', 'D_{in}');
        drawnow;

        for pob = POB
            pob.FaceColor = [0.1 0.1 0.1];
            pob.EdgeColor = [0.1 0.1 0.1];
        end

        if FInfo.state(t) >= 2
            POB(FInfo.nearo(t)).FaceColor = [0.6 0 0];
            POB(FInfo.nearo(t)).EdgeColor = [0.3 0 0];
        end
        if finalVideo
            writeVideo(V, getframe(gcf));
        end
    end
    figure(fTraj);
    drawnow;
    if finalVideo
        for i=1:V.FrameRate*2
            writeVideo(V, getframe(gcf));
        end
        close(V);
    end

    set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');
    set(findall(gcf,'-property','FontSize'),'FontSize',12);
    %grid on;
end