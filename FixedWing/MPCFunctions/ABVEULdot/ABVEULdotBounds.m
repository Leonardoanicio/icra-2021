function [Aeq, Beq, Aineq, Bineq] = ABVEULdotBounds(x, T, S, Coeffs_P, Coeffs_M, Beta, controls, ...
                                        nx, nu, UpperBounds, LowerBounds)   

    AEQ = (eye(6) - Coeffs_M*Coeffs_P)*Beta;
    AEQ = licols(AEQ')';
    Aeq = kron(eye(nu), AEQ);
    Beq = zeros(size(Aeq, 1), 1);
    
    AINEQ1 = Coeffs_P*Beta;
    AINEQ1 = kron(tril(ones(nu, nu)), AINEQ1);
    
    BINEQ1U = repmat(UpperBounds - controls, nu, 1);
    BINEQ1L = repmat(LowerBounds - controls, nu, 1);
    
    AINEQ2 = kron(eye(nu), Coeffs_P*Beta);
    
    BINEQ2U = 0.01*ones(4*nu, 1);
    BINEQ2L = -0.01*ones(4*nu, 1);
    
    K1 = zeros(1, 12);
    K1(1, 1) = 1;
    K1 = kron(eye(nx), K1); %-> Seleciona alpha
    
    K2 = zeros(1, 12);
    K2(1, 3) = 1;
    K2 = kron(eye(nx), K2); %-> Seleciona vt
    
    K3 = zeros(1, 12);
    K3(1, 5) = 1;
    K3 = kron(tril(ones(nx, nx)), K3)*0.01; % -> Seleciona sum(dtheta)*dt
    
    K = [K1; K2; K3];
    
    AINEQ3 = K*S;
    
    BINEQ3U = [0.279*ones(nx, 1); 28*ones(nx, 1); pi/2.1*ones(nx, 1)] - K*T*x;
    BINEQ3L = [-0.0349*ones(nx, 1); 18*ones(nx, 1); -pi/2.1*ones(nx, 1)] - K*T*x;
%     BINEQ3U = [pi/2.1*ones(nx, 1); 30*ones(nx, 1); pi/2.1*ones(nx, 1)] - K*T*x;
%     BINEQ3L = [-pi/2.1*ones(nx, 1); 15*ones(nx, 1); -pi/2.1*ones(nx, 1)] - K*T*x;
%     
%     BINEQ3U = repmat([0.279; 30; pi/(2 + 1e-3)], nx , 1);
%     BINEQ3U = BINEQ3U - K*T*x;
%     BINEQ3L = repmat([-0.0349; 18; -pi/(2 + 1e-3)], nx, 1);
%     BINEQ3L = BINEQ3L - K*T*x;
    
    Aineq = [AINEQ1; -AINEQ1; AINEQ2; -AINEQ2; AINEQ3; -AINEQ3];
    Bineq = [BINEQ1U; -BINEQ1L; BINEQ2U; -BINEQ2L; BINEQ3U; -BINEQ3L];   
    

    %Região terminal
    %dphi = 0, dtheta = 0 -> roll e pitch constantes
    K = zeros(2, 12*nx);
    K(1, end - 8) = 1;
    K(2, end - 7) = 1;
    A1 = K*S;
    B1 = K*T*x;
    AINEQ4 = A1;
    BINEQ4U = 1e-3*ones(2, 1) - B1;
    BINEQ4L = -1e-3*ones(2, 1) - B1;
    
    %ddphi = 0, ddtheta = 0, ddpsi = 0 -> velocidades angulares constantes
    %dalpha = 0, dbeta = 0, dvt = 0 -> velocidades lineares constantes
    K = zeros(6, 12*nx);
    K(1:6, end - 5:end) = 0.01*eye(6);
    A2 = K*S;
    B2 = K*T*x;
    AINEQ5 = A2;
    BINEQ5U = 1e-3*ones(6, 1) - B2;
    BINEQ5L = -1e-3*ones(6, 1) - B2;
%     
%    Aineq = [Aineq; AINEQ4; -AINEQ4; AINEQ5; -AINEQ5];
%    Bineq = [Bineq; BINEQ4U; -BINEQ4L; BINEQ5U; -BINEQ5L];
end