function [Alpha, Beta] = FLABVEULdot(states, C, UAV)    
    [pn, pe, h, ur, vr, wr, phi, theta, psi, p, q, r] = deal(states{:});

    fx_ex = C(1);
    fy_ex = C(2);
    fz_ex = C(3);
    l_ex = C(4);
    m_ex = C(5);
    n_ex = C(6);
    
    vt = norm([ur, vr, wr]);
    
    g = 9.81;
    
    Delta = [
        [-0.1e1 / UAV.uavMass * wr / (ur ^ 2 + wr ^ 2) 0 ur / UAV.uavMass / (ur ^ 2 + wr ^ 2) 0 0 0];
        [-vr * ur / UAV.uavMass / vt ^ 2 * (ur ^ 2 + wr ^ 2) ^ (-0.1e1 / 0.2e1) (-vr ^ 2 + vt ^ 2) / UAV.uavMass / vt ^ 2 * (ur ^ 2 + wr ^ 2) ^ (-0.1e1 / 0.2e1) -vr / UAV.uavMass * wr / vt ^ 2 * (ur ^ 2 + wr ^ 2) ^ (-0.1e1 / 0.2e1) 0 0 0];
        [ur / UAV.uavMass / vt vr / UAV.uavMass / vt 0.1e1 / UAV.uavMass * wr / vt 0 0 0];
        [0 0 0 (sin(theta) * UAV.Jxz * cos(phi) + UAV.Jz * cos(theta)) / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) / cos(theta) 0.1e1 / UAV.Jy * sin(theta) / cos(theta) * sin(phi) (sin(theta) * UAV.Jx * cos(phi) + UAV.Jxz * cos(theta)) / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) / cos(theta)];
        [0 0 0 -UAV.Jxz / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) * sin(phi) 0.1e1 / UAV.Jy * cos(phi) -UAV.Jx / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) * sin(phi)];
        [0 0 0 UAV.Jxz / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) / cos(theta) * cos(phi) 0.1e1 / UAV.Jy / cos(theta) * sin(phi) UAV.Jx / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) / cos(theta) * cos(phi)];
        ];

    B = [
        (cos(theta) * cos(phi) * g * UAV.uavMass * ur + sin(theta) * g * UAV.uavMass * wr + (-p * ur * vr + q * ur ^ 2 + q * wr ^ 2 - r * vr * wr) * UAV.uavMass + fz_ex * ur - fx_ex * wr) / (ur ^ 2 + wr ^ 2) / UAV.uavMass;
        (-((vr ^ 2 - vt ^ 2) * sin(phi) + cos(phi) * vr * wr) * g * UAV.uavMass * cos(theta) + sin(theta) * g * UAV.uavMass * ur * vr - fy_ex * vr ^ 2 + (-fx_ex * ur - fz_ex * wr) * vr + vt ^ 2 * ((p * wr - r * ur) * UAV.uavMass + fy_ex)) * (ur ^ 2 + wr ^ 2) ^ (-0.1e1 / 0.2e1) / vt ^ 2 / UAV.uavMass;
        (g * UAV.uavMass * (cos(phi) * wr + sin(phi) * vr) * cos(theta) - sin(theta) * g * UAV.uavMass * ur + fx_ex * ur + fy_ex * vr + fz_ex * wr) / vt / UAV.uavMass;
        (UAV.Jy * (-0.2e1 * q * r * (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) * cos(phi) ^ 2 - sin(phi) * (q - r) * (q + r) * (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) * cos(phi) - q * (UAV.Jxz * p - UAV.Jz * r) * UAV.Jy - 0.2e1 * q * r * UAV.Jxz ^ 2 + (p * (UAV.Jx + UAV.Jz) * q + n_ex) * UAV.Jxz + UAV.Jz * (q * (UAV.Jx - UAV.Jz) * r + l_ex)) * cos(theta) ^ 2 - sin(theta) * (-UAV.Jy * (-q * (UAV.Jx * p - UAV.Jxz * r) * UAV.Jy + (-q * (UAV.Jx + UAV.Jz) * r + l_ex) * UAV.Jxz + (p * (UAV.Jx + UAV.Jz) * q + n_ex) * UAV.Jx) * cos(phi) + (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) * sin(phi) * (p * r * UAV.Jy + UAV.Jxz * (p ^ 2 - r ^ 2) + p * (UAV.Jx - UAV.Jz) * r - m_ex)) * cos(theta) + 0.4e1 * (cos(phi) ^ 2 * q * r + sin(phi) * (q ^ 2 / 0.2e1 - r ^ 2 / 0.2e1) * cos(phi) - q * r / 0.2e1) * UAV.Jy * (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2)) / cos(theta) ^ 2 / UAV.Jy / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2);
        ((-(UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) * (p * r * UAV.Jy + UAV.Jxz * (p ^ 2 - r ^ 2) + p * r * UAV.Jx - p * r * UAV.Jz - m_ex) * cos(phi) - UAV.Jy * sin(phi) * (-q * (UAV.Jx * p - UAV.Jxz * r) * UAV.Jy + (-UAV.Jx * q * r - UAV.Jz * q * r + l_ex) * UAV.Jxz + UAV.Jx * (UAV.Jx * p * q + UAV.Jz * p * q + n_ex))) * cos(theta) + sin(theta) * UAV.Jy * ((q ^ 2 - r ^ 2) * cos(phi) ^ 2 - 0.2e1 * sin(phi) * q * r * cos(phi) - q ^ 2) * (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2)) / cos(theta) / UAV.Jy / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2);
        ((-(UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) * (p * r * UAV.Jy + UAV.Jxz * (p ^ 2 - r ^ 2) + p * r * UAV.Jx - p * r * UAV.Jz - m_ex) * sin(phi) + cos(phi) * UAV.Jy * (-q * (UAV.Jx * p - UAV.Jxz * r) * UAV.Jy + (-UAV.Jx * q * r - UAV.Jz * q * r + l_ex) * UAV.Jxz + UAV.Jx * (UAV.Jx * p * q + UAV.Jz * p * q + n_ex))) * cos(theta) + 0.4e1 * sin(theta) * (sin(phi) * (q ^ 2 / 0.2e1 - r ^ 2 / 0.2e1) * cos(phi) + q * r * (cos(phi) ^ 2 - 0.1e1 / 0.2e1)) * UAV.Jy * (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2)) / cos(theta) ^ 2 / UAV.Jy / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2);
        ];

    Beta = inv(Delta);
    Alpha = -Beta*B;
end