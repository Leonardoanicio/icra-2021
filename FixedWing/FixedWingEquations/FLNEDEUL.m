function [Alpha, Beta] = ABVPQR(states, C)
    [pn, pe, h, u, v, w, phi, theta, psi, p, q, r] = deal(states{:});
    global Jxz Jxy Jx Jy Jz M

    fx_ex = C(1);
    fy_ex = C(2);
    fz_ex = C(3);
    l_ex = C(4);
    m_ex = C(5);
    n_ex = C(6);
    
    vt = norm([u,v,w]);
    
    g = 9.81;
    uavMass = M;
    
    Delta = [
        -w/(uavMass*(u^2 + w^2)), 0., u/(uavMass*(u^2 + w^2)), 0., 0., 0.;
        -u/(uavMass*vt*sqrt(u^2 + w^2)), (vt - v)/(uavMass*vt*sqrt(u^2 + w^2)), -w/(uavMass*vt*sqrt(u^2 + w^2)), 0., 0., 0.;
        u/(uavMass*vt), v/(uavMass*vt), w/(uavMass*vt), 0., 0., 0.;
        0., 0., 0., Jz/(Jx*Jz - Jxz^2), 0., Jxz/(Jx*Jz - Jxz^2);
        0., 0., 0., 0., 1/Jy, 0.;
        0., 0., 0., Jxz/(Jx*Jz - Jxz^2), 0., Jx/(Jx*Jz - Jxz^2);
        ];

    B = [
        (cos(phi)*cos(theta)*g*u*uavMass + sin(theta)*g*uavMass*w + (-p*u*v + q*u^2 + q*w^2 - r*v*w)*uavMass + fz_ex*u - fx_ex*w)/((u^2 + w^2)*uavMass);
        (-uavMass*g*((-vt + v)*sin(phi) + cos(phi)*w)*cos(theta) + sin(theta)*g*u*uavMass + vt*(p*w - r*u)*uavMass - fy_ex*v + fy_ex*vt - fz_ex*w - fx_ex*u)/(uavMass*vt*sqrt(u^2 + w^2));
        (g*uavMass*(cos(phi)*w + sin(phi)*v)*cos(theta) - sin(theta)*g*u*uavMass + fx_ex*u + fy_ex*v + fz_ex*w)/(vt*uavMass);
        (-Jxz^2*q*r + (p*(Jx - Jy + Jz)*q + n_ex)*Jxz + Jz*(r*(Jy - Jz)*q + l_ex))/(Jx*Jz - Jxz^2);
        (-Jxz*p^2 - r*(Jx - Jz)*p + Jxz*r^2 + m_ex)/Jy;
        (-Jxz^2*p*q + (-r*(Jx - Jy + Jz)*q + l_ex)*Jxz + (p*(Jx - Jy)*q + n_ex)*Jx)/(Jx*Jz - Jxz^2);
        ];
    
    Beta = inv(Delta);
    Alpha = -Beta*B;
end