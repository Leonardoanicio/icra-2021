function [Aeq, Beq, Aineq, Bineq] = ABVPQRBounds(x, T, S, Coeffs_P, Coeffs_M, Beta, controls, ...
                                        nx, nu, UpperBounds, LowerBounds)   

    AEQ = (eye(6) - Coeffs_M*Coeffs_P)*Beta;
    Aeq = kron(eye(nu), AEQ);
    Beq = zeros(6*nu, 1);
    
    AINEQ1 = Coeffs_P*Beta;
    AINEQ1 = kron(tril(ones(nu, nu)), AINEQ1);
    
    BINEQ1U = repmat(UpperBounds - controls, nu, 1);
    BINEQ1L = repmat(LowerBounds - controls, nu, 1);
    
    AINEQ2 = kron(eye(nu), Coeffs_P*Beta);
    
    BINEQ2U = 0.01*ones(4*nu, 1);
    BINEQ2L = -0.01*ones(4*nu, 1);
    
    K1 = zeros(1, 12);
    K1(1, 1) = 1;
    K1 = kron(eye(nx), K1); %-> Seleciona alpha
    
    K2 = zeros(1, 12);
    K2(1, 3) = 1;
    K2 = kron(eye(nx), K2); %-> Seleciona vt
   
    K = [K1; K2];
    
    AINEQ3 = K*S;
    
    BINEQ3U = [0.279*ones(nx, 1); 28*ones(nx, 1)] - K*T*x;
    BINEQ3L = [-0.0349*ones(nx, 1); 18*ones(nx, 1)] - K*T*x;
    
    Aineq = [AINEQ1; -AINEQ1; AINEQ2; -AINEQ2; AINEQ3; -AINEQ3];
    Bineq = [BINEQ1U; -BINEQ1L; BINEQ2U; -BINEQ2L; BINEQ3U; -BINEQ3L];   
    
    %dp = 0, dq = 0, dr = 0 -> velocidades angulares constantes
    %dalpha = 0, dbeta = 0, dvt = 0 -> velocidades lineares constantes
    K = zeros(3, 12*nx);
    K(1:3, end - 5:end-3) = 0.01*eye(3);
    A2 = K*S;
    B2 = K*T*x;
    AINEQ5 = A2;
    BINEQ5U = 1e-3*ones(3, 1) - B2;
    BINEQ5L = -1e-3*ones(3, 1) - B2;
    
%    Aineq = [Aineq; AINEQ5; -AINEQ5];
%    Bineq = [Bineq; BINEQ5U; -BINEQ5L];
end