%{
IMU POS -> 1e-6
IMU VEL -> 2.5e-5
GPS -> 2e-3
Barometro -> 2.5e-2
%}

impedance = 2.5e-2;
x = wgn(1, 100000, 1, impedance);
H = histogram(x, 'Normalization', 'pdf');
sigma = std(x);
hold on;
plot([-3*sigma -3*sigma], [0 max(H.Values)], 'r', [3*sigma 3*sigma], [0 max(H.Values)], 'r');
title(sprintf("%.3e", 3*sigma));