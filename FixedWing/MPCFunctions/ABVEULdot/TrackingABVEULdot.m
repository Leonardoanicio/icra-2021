function [X, U, E, Re, FInfo, Tout, t, LinSys, flag, EKF_OUT] = TrackingABVEULdot(GroundTruth, controls, MPC, ...
                                                            UAV, tf, dt, curveType, Wind)
                                               
    Hqp = MPC.Hqp;
    Fqp = MPC.Fqp;
    S = MPC.S;
    T = MPC.T;
    nx = MPC.nx;
    nu = MPC.nu;
    options = MPC.options;
    
    %Configurações do campo
    tbeta = 0;
    circDir = 1; % recalculated below
    fstate = 'follow curve'; % recalculated below
    
    %Vetores para salvar os dados
    U = zeros(4, tf/dt);
    LinSys.DV = zeros(6, tf/dt);
    LinSys.X = zeros(12, tf/dt);
    LinSys.noise = zeros(6, tf/dt);
    E = zeros(6, tf/dt);
    Re = zeros(6, tf/dt);
    X = zeros(12, tf/dt);
    Tout = zeros(1, tf/dt);
    index = [7 8 9]; %phi theta psi
    
    %Informações do campo
    FInfo.nearo = zeros(1, tf/dt);
    FInfo.D = zeros(1, tf/dt);
    FInfo.state = zeros(1, tf/dt);
    FInfo.cirdir = zeros(1, tf/dt);
    FInfo.tbeta = zeros(1, tf/dt);
    FInfo.F = zeros(3, tf/dt);
    FInfo.Fc = zeros(3, tf/dt);
    FInfo.Fo = zeros(3, tf/dt);

    %Carrega os estados iniciais
    X(:, 1) = GroundTruth;
    U(:, 1) = controls;
    xdot = zeros(12, 1);
    alphaold = 0;
    EKF_alphaold = alphaold;

    %Passa de vetor para celula
    EKF_States = GroundTruth(1:9);
    GroundTruth = num2cell(GroundTruth);
    
    %EKF
    global EKF_P
    EKF_OUT = zeros(9, size(X, 2));
    EKF_OUT(:, 1) = EKF_States;
    EKF_UAV = UAV;
    EKF_P = ones(9, 1)*0.01;
    EKF_Q = [0 0 0,...
             0.0025*9.81 0.0025*9.81 0.0025*9.81,...
             0.0026 0.0026 0.0026].^2;
    EKF_R = [0.05 0.05 0.2];
    EKF_P = diag(EKF_P);
    load("EKF_P");
    EKF_Q = diag(EKF_Q);
    EKF_R = diag(EKF_R);
    
    t_correct = 120e-3;
    t_predict = 10e-3;
    
    %Simulacao
    i = 1;
    simdt = 1e-3;
    for t=0:simdt:tf
        %Calcula as matrizes do modelo real
        [Real_M, Real_C, ~, alphaold] = getMatrices(GroundTruth, controls, UAV, alphaold);
        
         if i == 1
            oldGPS = cell2mat(GroundTruth(1:3));
            oldAccel = Real_M*controls + Real_C;
            oldAccel = oldAccel(1:3)/UAV.uavMass;
            oldGyro = cell2mat(GroundTruth(10:12));
            
            GPS = oldGPS;
        end
        
        if mod(t, t_predict) == 0 %Passo de predição
            Gyro = cell2mat(GroundTruth(10:12)); %Pega pqr
            
            Accel = Real_M*controls + Real_C;
            Accel = Accel(1:3)/UAV.uavMass; %Pega as acelerações lineares
            
            [EKF_P, EKF_States, EKF_Derivatives, oldGyro, oldAccel, ~] = EKF(EKF_P, EKF_States, Gyro, Accel, GPS,...
            oldGyro, oldAccel, oldGPS, EKF_R, EKF_Q, false, Wind);
            EKF_xdot = EKF_Derivatives(index);
        end
        if mod(t, t_correct) == 0%Passo de correção
            GPS = cell2mat(GroundTruth(1:3));
            [EKF_P, EKF_States, ~, ~, ~, oldGPS] = EKF(EKF_P, EKF_States, Gyro, Accel, GPS,...
            oldGyro, oldAccel, oldGPS, EKF_R, EKF_Q, true, Wind);
        end
        if mod(t, dt) == 0 %Calula o controle
            tic;
            
            States = num2cell([EKF_States; oldGyro]); %Adiciona PQR aos estados
            %DESCOMENTE AS DUAS PARA NÃO USAR O EKF
            States = GroundTruth;
            EKF_xdot = xdot(index);
            
            %Calcula as matrizes do estimador
            [EKF_M, EKF_C, ~, EKF_alphaold] = getMatrices(States, controls, UAV, EKF_alphaold);
            EKF_Pinv = (EKF_M'*EKF_M)\EKF_M';

            %Distribui os estados
            [pn, pe, pd, ur, vr, wr, phi, theta, psi, p, q, r] = States{:};

            %Alpha, Beta, Va
            alpha = atan2(wr,ur);
            vt = norm([ur,vr,wr]);
            beta = asin(vr/vt);

            %Feedback Linearization
            [Alpha, Beta] = FLABVEULdot(States, EKF_C, EKF_UAV);

            %Estados atuais -> modelo linear
            x = [alpha; beta; vt; EKF_xdot(1); EKF_xdot(2); EKF_xdot(3); Beta\(EKF_M*controls - Alpha)];
            LinSys.X(:, i) = x;

            %Horizonte de predição
            [xob, fstate, tbeta, circDir, nearObstacle, D, F, Fc, Fo] = ABVEULdotHorizon(States, nx, dt,...
                                                           fstate, tbeta, circDir, curveType, t, Wind);

            FInfo.F(:, i) = F;
            FInfo.Fc(:, i) = Fc;
            FInfo.Fo(:, i) = Fo;
            FInfo.circdir(i) = circDir;
            FInfo.tbeta(i) = tbeta;
            FInfo.nearo(i) = nearObstacle;
            FInfo.D(i) = D;
            switch fstate
                case 'follow curve'
                    FInfo.state(i) = 1;
                case 'transition to obstacle'
                    FInfo.state(i) = 2;
                case 'contour obstacle'
                    FInfo.state(i) = 3;
                case 'transition to curve'
                    FInfo.state(i) = 4;
            end

            %Restricao dos estados
            [Aeq, Beq, Aineq, Bineq] = ABVEULdotBounds(x, T, S, EKF_Pinv, EKF_M, Beta, controls, ...
                nx, nu, UAV.UpperBounds, UAV.LowerBounds);

            %Quadprog
            f = 2*(x'*T' - xob')*Fqp; 
            f(7:end) = 0;
            [delta_v, flag] = mpcActiveSetSolver(Hqp, f', Aineq, Bineq, Aeq, Beq, false(size(Bineq)), options);
           
            Tout(i) = toc;

            if flag <= 0
                flag
                break
            end

            %Transforma para os controles
            controls = controls + EKF_Pinv*Beta*delta_v(1:6);
        end

        %Evolui o modelo
        xdot = EOM(GroundTruth, Real_M*controls + Real_C, UAV, Wind);

        GroundTruth = cell2mat(GroundTruth) + xdot*simdt;
        GroundTruth = num2cell(GroundTruth);

        if mod(i, 100) == 0
            clc;
            fprintf('Simulating\n%.2fs', t);
        end

        if mod(t, dt) == 0
            %Salva os dados
            U(:, i) = controls;
            EKF_OUT(:, i) = EKF_States;
            LinSys.DV(:, i) = delta_v(1:6);
            X(:, i) = cell2mat(GroundTruth); %-> Modelo não linear

            %Salva os erros
            E(:,i) = xob(1:6) - x(1:6);
            Re(:,i) = xob(1:6); 
            i = i + 1;
        end
    end
    
    if flag > 0
        clc;
    end
    fprintf('Max Time = %.4f\n', max(Tout));
    fprintf('Mean Time = %.4f\n', mean(Tout));
    
    
end