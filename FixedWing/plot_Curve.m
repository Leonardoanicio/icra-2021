plot_migue_pra_legenda = plot3(1e10,0,0, 'k-', 'LineWidth', 3);
rc = plot_alpha(curve_type);
rc.LineWidth = 3;


hold on;
pe = X(2, :);
pn = X(1, :);
pd = X(3, :);
aa = find(FInfo.state == 1);
bb = find(FInfo.state == 2 | FInfo.state == 4);
cc = find(FInfo.state == 3);

plot3(pe(aa), pn(aa), -pd(aa), '.', 'Color', [0 0 1]);
pa = plot3(0,0,0, 'Color', [0 0 1]);
plot3(pe(bb), pn(bb), -pd(bb), '.', 'Color', [0.7 0 0.7]);
pb = plot3(0,0,0, 'Color', [0.7 0 0.7]);
plot3(pe(cc), pn(cc), -pd(cc), '.', 'Color', [1 0 0]);
pc = plot3(0,0,0, 'Color', [1 0 0]);
           

%Pega a curva
[f_a1, f_a2, P, a_curve, b_curve, c_curve, O, O_var, kp] = getCurve(curve_type);

bigFuckingObstacle = find(abs(diff(O(1, :))) < 10 & abs(diff(O(2, :))) < 10);

%Plota os obstaculos
begin = false;
angle = linspace(0, 2*pi, 50);
zmin = min(-pd);
zz = zmin*ones(size(angle));
axis([min(pe)-100 max(pe)+100 min(pn)-100 max(pn)+100 min(-pd)-25 max(-pd)+25]);        
for i=1:length(FInfo.state)
    if FInfo.state(i) == 3 && begin == false
        near = FInfo.nearo(i);
        
        if ismember(near, bigFuckingObstacle)
            near = bigFuckingObstacle;
        end
        
        t = i*dt;
        Oi = O(:, near);
        O_vari = [];
        
        for n = near
            O_vari = [O_vari, [O_var{1, n}(t); O_var{2, n}(t)]];
        end
        
        x1 = Oi(1, :) + O_vari(1, :);
        y1 = Oi(2, :) + O_vari(2, :);
        r = Oi(4);

        begin = true;
    elseif FInfo.state(i) == 4 && begin == true
        begin = false;
        
        near = FInfo.nearo(i);
        
        if ismember(near, bigFuckingObstacle)
            near = bigFuckingObstacle;
        end
        
        t = i*dt;
        Oi = O(:, near);
        O_vari = [];
        
        for n = near
            O_vari = [O_vari, [O_var{1, n}(t); O_var{2, n}(t)]];
        end
        
        x2 = Oi(1, :) + O_vari(1, :);
        y2 = Oi(2, :) + O_vari(2, :);
        r = Oi(4);
        
        Xo = x1 + (x2 - x1)/2;
        Yo = y1 + (y2 - y1)/2;
        
        for j=1:length(Xo)
            x = Xo(j);
            y = Yo(j);
            %Plota o obstáculo
            ob = plotCylinder(x, y, 300, r, 'black', 0.5);

            %Curva de referencia
            cr = plotCylinder(x, y, 300, r+50, 'yellow', 0.5);

            %Din
            din =plotCylinder(x, y, 300, r+150, 'green', 0.2);
        end
    end
end

axis equal
xlim([min(pe)-25 max(pe)+25])
ylim([min(pn)-25 max(pn)+25])
zlim([-max(pd)-25 -min(pd)+25])
grid on
legend([plot_migue_pra_legenda, ob, cr, din, pa, pb, pc], 'Target Curve', 'Obstacle', 'Obstacle Curve', 'D_{in}', 'Follow Curve', 'Transition', 'Contour Obstacle');
