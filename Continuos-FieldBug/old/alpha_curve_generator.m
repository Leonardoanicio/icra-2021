syms x y z a b c

alpha1 = x^2 / a^2 + z^2 / c^2 - 1;
alpha2 = y - (2*b/(a*c))*x*z;

grad1 = [diff(alpha1, x); diff(alpha1, y); diff(alpha1, z)];
%   (2*x)/a^2
%          0
%  (2*z)/c^2

grad2 = [diff(alpha2, x); diff(alpha2, y); diff(alpha2, z)];
%  -(2*b*z)/(a*c)
%               1
%  -(2*b*x)/(a*c)

g1_vec_g2 = cross(grad1, grad2);
%                             -(2*z)/c^2
%  (4*b*x^2)/(a^3*c) - (4*b*z^2)/(a*c^3)
%                              (2*x)/a^2

E = (0.5*alpha1^2 + 0.5*alpha2^2);
% (y - (2*b*x*z)/(a*c))^2/2 + (x^2/a^2 + z^2/c^2 - 1)^2/2
gradE = [diff(E, x); diff(E, y); diff(E, z)];
%  (2*x*(x^2/a^2 + z^2/c^2 - 1))/a^2 - (2*b*z*(y - (2*b*x*z)/(a*c)))/(a*c)
%                                                      y - (2*b*x*z)/(a*c)
%  (2*z*(x^2/a^2 + z^2/c^2 - 1))/c^2 - (2*b*x*(y - (2*b*x*z)/(a*c)))/(a*c)