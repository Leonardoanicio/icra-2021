function f = obstacle_vector_alpha(p, Oi, Oix_var, Oiy_var, r, dir, time, vr)
    
    kp = 2.5;   
    dt = 1e-6;
    
    R = 50 + Oi(4);
    
    dalphadx = 2*(p(1) - Oi(1) - Oix_var(time))/R^2;
    dalphady = 2*(p(2) - Oi(2) - Oiy_var(time))/R^2;
    
    alpha_0 = (p(1) - Oi(1) - Oix_var(time))^2/R^2 + (p(2) - Oi(2) - Oiy_var(time))^2/R^2 - 1;
    alpha_Mt = (p(1) - Oi(1) - Oix_var(time - dt))^2/R^2 + (p(2) - Oi(2) - Oiy_var(time - dt))^2/R^2 - 1;
    dalphadt = (alpha_0 - alpha_Mt)/dt;
    
    G = -2/pi * atan(kp*alpha_0); H = real(sqrt(1 - G^2));
    
    Da = [dalphadx dalphady]';
    DHa = [-dalphady dalphadx]';
    
    M = [Da'; DHa'];
    
    a = [dalphadt 0]';
    
    f_vt = -inv(M)*a;
    f_s = (G*Da + dir*H*DHa)/(norm(Da) + 1e-6);
    n = -f_s'*f_vt + sqrt(vr^2 + (f_s'*f_vt)^2 - norm(f_vt)^2 );
    
    f =  n*f_s + f_vt;
    
    f = [f;0];
end
