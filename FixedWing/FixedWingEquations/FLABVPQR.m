function [Alpha, Beta] = FLABVPQR(states, C, UAV)
    [pn, pe, h, u, v, w, phi, theta, psi, p, q, r] = deal(states{:});
    
    fx_ex = C(1);
    fy_ex = C(2);
    fz_ex = C(3);
    l_ex = C(4);
    m_ex = C(5);
    n_ex = C(6);
    
    vt = norm([u,v,w]);
    
    g = 9.81;
    
    Delta = [-0.1e1 / UAV.uavMass * w / (u ^ 2 + w ^ 2) 0 u / UAV.uavMass / (u ^ 2 + w ^ 2) 0 0 0;
        -u / UAV.uavMass / vt * (u ^ 2 + w ^ 2) ^ (-0.1e1 / 0.2e1) (vt - v) / UAV.uavMass * (u ^ 2 + w ^ 2) ^ (-0.1e1 / 0.2e1) / vt -0.1e1 / UAV.uavMass * w / vt * (u ^ 2 + w ^ 2) ^ (-0.1e1 / 0.2e1) 0 0 0;
        u / UAV.uavMass / vt v / UAV.uavMass / vt 0.1e1 / UAV.uavMass * w / vt 0 0 0;
        0 0 0 UAV.Jz / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) 0 UAV.Jxz / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2);
        0 0 0 0 0.1e1 / UAV.Jy 0; 0 0 0 UAV.Jxz / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2) 0 UAV.Jx / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2);
        ];

    
    B = [(cos(phi) * cos(theta) * g * u * UAV.uavMass + sin(theta) * g * UAV.uavMass * w + (-p * u * v + q * u ^ 2 + q * w ^ 2 - r * v * w) * UAV.uavMass + fz_ex * u - fx_ex * w) / (u ^ 2 + w ^ 2) / UAV.uavMass;
        (-UAV.uavMass * g * ((-vt + v) * sin(phi) + cos(phi) * w) * cos(theta) + sin(theta) * g * u * UAV.uavMass + vt * (p * w - r * u) * UAV.uavMass - fy_ex * v + fy_ex * vt - fz_ex * w - fx_ex * u) * (u ^ 2 + w ^ 2) ^ (-0.1e1 / 0.2e1) / vt / UAV.uavMass;
        (g * UAV.uavMass * (cos(phi) * w + sin(phi) * v) * cos(theta) - sin(theta) * g * u * UAV.uavMass + fx_ex * u + fy_ex * v + fz_ex * w) / vt / UAV.uavMass;
        (-UAV.Jxz ^ 2 * q * r + (p * (UAV.Jx - UAV.Jy + UAV.Jz) * q + n_ex) * UAV.Jxz + (r * (UAV.Jy - UAV.Jz) * q + l_ex) * UAV.Jz) / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2);
        (-UAV.Jxz * p ^ 2 - r * (UAV.Jx - UAV.Jz) * p + UAV.Jxz * r ^ 2 + m_ex) / UAV.Jy;
        (UAV.Jxz ^ 2 * p * q + (-r * (UAV.Jx - UAV.Jy + UAV.Jz) * q + l_ex) * UAV.Jxz + (p * (UAV.Jx - UAV.Jy) * q + n_ex) * UAV.Jx) / (UAV.Jx * UAV.Jz - UAV.Jxz ^ 2)];
    
    Beta = inv(Delta);
    Alpha = -Beta*B;
end