[XX, Y, Z] = meshgrid(linspace(-650,650,300));
switch curve_type
    case 1
        a = 600;
        b = 400;
        u = 50;
        W = (XX./a).^2 + (Y./b).^2 - 1;
        Z = 200 - u*((XX/a).^2 - 1);
        
    case 3
        a = 600;
        b = 100*a^3;
        c = 1e4;
        W = (XX.^2 + Y.^2).*(Y.^2 + XX.*(XX+a)) - 4.*a.*XX.*Y.*Y - b;
        Z = 200 + (XX.^2 + Y.^2)/c;
end
        
[face,verts] = isosurface(XX, Y, Z, W, 0);

clear XX Y Z W

Dc = [];
for j = 1:length(X)
    x = X(:, j);
    pos = [x(2); x(1); -x(3)]';
    D = vecnorm(verts - pos, 2, 2);
    i = find(D == min(D), 1);
    Dc = [Dc D(i)];
end
plot(t, Dc, 'LineWidth', 2); hold;
plot(t, FInfo.D(1:index), 'LineWidth', 2)
grid
set(findall(gcf,'-property','FontSize'),'FontSize',15);
xlabel('Time (s)', 'FontSize', 18)
ylabel('Distance (m)', 'FontSize', 18);
title('UAV Distances', 'FontSize', 18);
set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');
legend('Distance To Desired Curve', 'Distance To Nearest Obstacle', 'FontSize', 15)
