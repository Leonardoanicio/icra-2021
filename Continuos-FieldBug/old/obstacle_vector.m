function f = obstacle_vector(p, Oi, r, dir)

    epsilon = 40;

    D = -r(1:2);
    
    E = D - epsilon/norm(D) * D;
    
    kp = 0.0025;   
    
%     G = -2/pi * atan(kp*norm(D));
    G = -tanh(kp*norm(D));
    H = sqrt(1 - G^2);
    
    R = [0 -1; 1 0];
    
    f = G*E/norm(E) + dir*H*R*D/norm(D);
    
    f = [f;0];
end