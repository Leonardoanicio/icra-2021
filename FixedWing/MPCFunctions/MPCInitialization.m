function [Hqp, Fqp, S, T, options] = MPCInitialization(nx, nu, dt, feedType)
    
    [A, B, Q, R, P] = Weights(feedType, dt);
    [Hqp, Fqp, Qb, T, S] = MPCFormulation(A, B, Q, R, P, nx, nu);

    %Quadprog
%     options = optimoptions('quadprog', 'Algorithm', 'active-set', 'TolFun', 1e-6, 'Display', 'off');
%     options.MaxIterations = 1e3;
    
    options = mpcActiveSetOptions('double');
%     options.ConstraintTolerance = 1.0e-4;
%     options.IntegrityChecks = false;
end