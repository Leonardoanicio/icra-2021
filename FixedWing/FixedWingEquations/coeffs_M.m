function M_6x4 = coeffs_M(alpha, beta, vt, ail, elev, rud, rho, UAV)
    selev = sign(elev);
    if selev == 0
        selev = 1;
    end
    
    sail= sign(ail);
    if sail == 0
        sail = 1;
    end
    
    srud = sign(rud);
    if srud == 0
        srud = 1;
    end

    CD_delta_ail_local = UAV.CD_delta_ail*sail;
    CD_delta_elev_local = UAV.CD_delta_elev*selev;
    CD_delta_rud_local = UAV.CD_delta_rud*srud;

    Fail = [
            -cos(alpha)*(cos(beta)*CD_delta_ail_local + sin(beta)*UAV.CY_delta_ail);
            (-sin(beta)*CD_delta_ail_local + cos(beta)*UAV.CY_delta_ail);
            -sin(alpha)*(cos(beta)*CD_delta_ail_local + sin(beta)*UAV.CY_delta_ail)
            ];
    Felev = [
            (-cos(beta)*cos(alpha)*CD_delta_elev_local + sin(alpha)*UAV.CL_delta_elev);
            -sin(beta)*CD_delta_elev_local;
            -(cos(beta)*sin(alpha)*CD_delta_elev_local + cos(alpha)*UAV.CL_delta_elev)
            ];
    Frud  = [
            -cos(alpha)*(cos(beta)*CD_delta_rud_local + sin(beta)*UAV.CY_delta_rud);
            (-sin(beta)*CD_delta_rud_local + cos(beta)*UAV.CY_delta_rud);
            -sin(alpha)*(cos(beta)*CD_delta_rud_local + sin(beta)*UAV.CY_delta_rud)
            ];
    Fthr = [
            UAV.F_T;
            0;
            0
            ];

    Forces = horzcat(0.5*rho*vt^2*UAV.Sw*Fail, 0.5*rho*vt^2*UAV.Sw*Felev,...
        0.5*rho*vt^2*UAV.Sw*Frud, Fthr);

    Mail = [
            UAV.Bw* UAV.Cl_delta_ail;
            0.;
            UAV.Bw* UAV.Cn_delta_ail
            ];
    Melev = [
            0.;
            UAV.Cw* UAV.Cm_delta_elev;
            0.
            ];
    Mrud = [
            UAV.Bw* UAV.Cl_delta_rud;
            0.;
            UAV.Bw* UAV.Cn_delta_rud
            ];
    Mthr = [0;
            0;
            0;
            ];
    Moments = horzcat(0.5*rho*vt^2*UAV.Sw*Mail, 0.5*rho*vt^2*UAV.Sw*Melev,...
        0.5*rho*vt^2*UAV.Sw*Mrud, Mthr);

    M_6x4 = vertcat(Forces, Moments);
    
end