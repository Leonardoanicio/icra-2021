%% Continuous Filedbug
% Need Adriano's matlab_lib added to path
% https://github.com/adrianomcr/matlab_lib

close all, clear, clc
%% Inputs

curve_type = 11; % 1-circle 2-race track  4-smooth square 5-crown 6-vertical circle 
%9-time varying circle 11 - Lemniscata (VANT)
%**3-perfect square **7-crazy loops **8-polar rose 
c_x = 0; c_y = 0; c_z = 0; %curve centers
r_x = 5; r_y = 5; r_z = 1; %curve alongations

p = [100 20 250]'; % Initial position
max_time = 100; %simulation time

ws = [-1 1 -1 1 -1 1]*700; % animation plot size
r_tilde = 15; % robot radius (just for plot)
animation_speed = 10; % just for plot

%Obstacles [cx cy cz radius] %cz is now irrelevant
O1 = [1 0 0 0.1]';
O3 = [0 -3.5 0 0.8]';
O2 = [1 3 0 0.5]';
O4= [0.8*3.5 0.8*3.5 0 0.3]';

O5 = [0 6.5 0 0.8]' ;
O6 = [0 -3.5 0 0.8]';
O7 = [-4.6 0 0 0.2]';
O8 = [-5 0 0 0.2]';
O9 = [-5.4 0 0 0.2]';
O10 = [-5.8 0 0 0.2]';
O11 = [-6.2 0 0 0.2]';
O12 = [-6.6 0 0 0.2]';
O13 = [-7.0 0 0 0.2]';
O14 = [-7.4 0 0 0.2]';
O15 = [-7.8 0 0 0.2]';

%O = [O1 O2 O3 O4];

O = [O2 O4 O5 O6 O7 O8 O9 O10 O11 O12 O13 O15];

MASTER_MODE = 1; % we will call auxiliar scripts
%% Selecting the curve 
fprintf('Program started\n')
curve %auxiliar script that can be run separately

dt = 10e-3;
t = 0:dt:max_time;
ds = pi/100;
s = 0:ds:2*pi;

C = fC(s,0);

%% Simulation

fprintf('Simulation started\n')
pause(1)

D_data = []; % for distance plot
f_data = []; % for |V| plot
state_data = []; % for plot

vr = 23; 
d_in = 120;
Tbeta = 10; % recalculated below
circ_dir = 1; % recalculated below
state = 'follow curve'; % recalculated below

% O_points = {}; % Obstacle "detectable" points
% for k = 1:size(O,2)
%      Oi = O(:,k);
%      eQ = @(r) [Oi(4)*cos(r) + Oi(1); Oi(4)*sin(r) + Oi(2)];
%      sample = 50;
%      theta = linspace(0,2*pi, sample);
%      O_points(k) = {eQ(theta)};
% end

for k = 1:length(t)
    clc; fprintf('Simulating...\n%d%%\n',round(100*t(k)/t(end)))
    
    % find the nearest obstacle and the vector r
    near_obst = 1;
    r = [inf, inf];
    for j = 1:size(O,2)
        new_r = O(1:2, j) - p(1:2,k);
        new_r = new_r - O(4,j)*new_r/norm(new_r);
        if norm(r) > norm( new_r ) 
           r = new_r;
           near_obst = j;
        end
    end
    
    %d_in calculado usando o angulo da tangente interna
    %d_in = dinInnerTangent(norm(r), r_tilde, pi/12);
    
%     for j = 1:size(O,2)
%         for i = 1:sample
%             if norm(r) > norm(  O_points{j}(:,i) - p(1:2,k) )
%                 r = O_points{j}(:,i) - p(1:2,k);
%                 near_obst = j;
%             end
%         end
%     end
    
    %d_in = (r_tilde + O(4, near_obst) + 30)/sin(pi/8);

    Tbeta = 0.4*(d_in - O(4,near_obst))/vr;
    r = [r;0];
    
    switch state
        case 'follow curve'
            [fc, D] = curve_vector(p(:,k), fC, k);
            f = vr*fc;
            state_data(k) = 1;
            if norm(r) < d_in && r'*fc > 0
                state = 'transition to obstacle';
                Beta = 0;
                circ_dir = cross([r(1:2);0],[fc(1:2);0]);
                circ_dir = -sign(circ_dir(3));
            end
        case 'transition to obstacle'
            [fc, D] = curve_vector(p(:,k), fC, k);
%             [fc, D] = curve_vector_alpha(p(:, k));
            fo = obstacle_vector(p(:,k), O(:,near_obst), r, circ_dir);
            f = (1-Beta)*fc + Beta*fo;
            f(3) = fc(3); % keep following z component
            if norm(f) == 0
                f = [0;0;0];
            else
                f = vr/norm(f) * f;
            end
            
            Beta = Beta + dt/Tbeta;
            state_data(k) = 2;
            if Beta > 1
                state = 'contour obstacle';
            end
        case 'contour obstacle'
            [fc, D] = curve_vector(p(:,k), fC, k);
%             [fc, D] = curve_vector_alpha(p(:, k));
            fo = obstacle_vector(p(:,k), O(:,near_obst), r, circ_dir);
            f = vr*fo;
            state_data(k) = 3;
            f(3) = fc(3); % keep following z component
            if r'*fc < 0
                state = 'transition to curve';
                Beta = 0;
            end
        case 'transition to curve'
            [fc, D] = curve_vector(p(:,k), fC, k);
%             [fc, D] = curve_vector_alpha(p(:, k));
            fo = obstacle_vector(p(:,k), O(:,near_obst), r, circ_dir);
            
            f = Beta*fc + (1-Beta)*fo;
            f(3) = fc(3); % keep following z component
            if norm(f) == 0
                f = [0;0;0];
            else
                f = vr/norm(f) * f;
            end
            
            Beta = Beta + dt/Tbeta;
            state_data(k) = 4;
            if Beta > 1
                state = 'follow curve';
            end
        otherwise
            state = 'follow curve';  
    end
    
    p(:,k+1) = p(:,k) + f*dt;
    f_data(k) = norm(f);
    D_data(k) = norm(D);
end

%% Static results

%Trajectory
figure()
hold on
c = distinguishable_colors(1);
plot3(C(1,:),C(2,:),C(3,:),'k-','LineWidth',1)

aa = find(state_data == 1);
bb = find(state_data == 2 | state_data == 4);
cc = find(state_data == 3);
plot3(p(1,aa),p(2,aa),p(3,aa),'.','Color',[0 0 1])
plot3(p(1,bb),p(2,bb),p(3,bb),'.','Color',[0.7 0 0.7])
plot3(p(1,cc),p(2,cc),p(3,cc),'.','Color',[1 0 0])

plot3(p(1,end),p(2,end),p(3,end),'.','Color',[0 0 1],'LineWidth',1,'MarkerSize',25)

for k = 1:size(O,2)
    theta = linspace(0, 2*pi, 50);
    Oi = O(:,k);
    x = Oi(4)*cos(theta) + Oi(1); y = Oi(4)*sin(theta) + Oi(2);
    plot(x,y,'k--', 'LineWidth', 1.5);
end
hold off
grid on
axis equal
xlabel('$x$','interpreter','latex','FontSize',15)
ylabel('$y$','interpreter','latex','FontSize',15)
zlabel('$z$','interpreter','latex','FontSize',15)

%Distace
figure()
hold on
plot(t,D_data(:),'-','Color',c(1,:),'LineWidth',2)
grid on
xlabel('$t$','interpreter','latex','FontSize',15)
ylabel('$D$','interpreter','latex','FontSize',15)

%Velocity
figure()
hold on
plot(t,f_data(:),'.','Color',c(1,:))
grid on
xlabel('$t$','interpreter','latex','FontSize',15)
ylabel('$\|V\|$','interpreter','latex','FontSize',15)

%% Animation
% animation %auxiliar script that can be run separately
% clear MASTER_MODE a aa ans b bb Beta C cc circ_dir j k k0 near_obst new_r state tail Tbeta theta x y
% clear O0 O1 O2 O3 O4 O5 O6 O7 O8 O9 O10 O11 O12 O13 O14 O15
