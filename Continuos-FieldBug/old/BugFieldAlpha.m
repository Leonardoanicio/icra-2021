function [f, D, state, Beta, circ_dir, near_obst, fc, fo] = BugFieldAlpha(p, curveType, state, Beta, circ_dir)
    d_in = 150;
    vr = 23;
    dt = 0.01;
    
    p(3) = p(3) - 200;
    
    fc = zeros(3, 1);
    fo = zeros(3, 1);
    
    %Pega a curva
    [f_a1, f_a2, a_curve, b_curve, c_curve, O] = getCurve(curveType);
   
    % find the nearest obstacle and the vector r
    near_obst = 1;
    r = [inf; inf];
    for j = 1:size(O,2)
        new_r = O(1:2, j) - p(1:2);
        new_r = new_r - O(4,j)*new_r/norm(new_r);
        if norm(r) > norm( new_r ) 
           r = new_r;
           near_obst = j;
        end
    end
    
    if size(O, 2) > 0
        Tbeta = 0.5*(d_in - O(4,near_obst))/vr;
    else
        Tbeta = inf;
    end
    r = [r;0];
    
    vr = 1;
    
    switch state
        case 'follow curve'
            [fc, D] = curve_vector_alpha2(p, a_curve, b_curve, c_curve, f_a1, f_a2);
            f = vr*fc;
            if norm(r) < d_in && r'*fc > 0
                state = 'transition to obstacle';
                Beta = 0;
                circ_dir = cross([r(1:2);0],[fc(1:2);0]);
                circ_dir = -sign(circ_dir(3));
            end
        case 'transition to obstacle'
            [fc, D] = curve_vector_alpha2(p, a_curve, b_curve, c_curve, f_a1, f_a2);
            fo = obstacle_vector_alpha(p, O(:,near_obst), r, circ_dir);
            f = (1-Beta)*fc + Beta*fo;
            f(3) = fc(3); % keep following z component
            if norm(f) == 0
                f = [0;0;0];
            else
                f = vr/norm(f) * f;
            end
            
            Beta = Beta + dt/Tbeta;
            if Beta > 1
                state = 'contour obstacle';
            end
        case 'contour obstacle'
            [fc, D] = curve_vector_alpha2(p, a_curve, b_curve, c_curve, f_a1, f_a2);
            fo = obstacle_vector_alpha(p, O(:,near_obst), r, circ_dir);
            f = vr*fo;
            f(3) = fc(3); % keep following z component
            if r'*fc < 0
                state = 'transition to curve';
                Beta = 0;
            end
        case 'transition to curve'
            [fc, D] = curve_vector_alpha2(p, a_curve, b_curve, c_curve, f_a1, f_a2);
            fo = obstacle_vector_alpha(p, O(:,near_obst), r, circ_dir);
            
            f = Beta*fc + (1-Beta)*fo;
            f(3) = fc(3); % keep following z component
            if norm(f) == 0
                f = [0;0;0];
            else
                f = vr/norm(f) * f;
            end
            
            Beta = Beta + dt/Tbeta;
            if Beta > 1
                state = 'follow curve';
            end
        otherwise
            state = 'follow curve';  
    end
end

