dx = diff(X(2, :)/0.01);
dy = diff(X(1, :)/0.01);
dz = -diff(X(3, :)/0.01);
fz = FInfo.F(3, 1:end - 1);
fy = FInfo.F(2, 1:end - 1);
fx = FInfo.F(1, 1:end - 1);

t = linspace(0, 200, length(dx));

subplot(3, 1, 1, 'Units', 'normalized', 'Position', [0.05, 0.65, 0.9, 0.275]);
plot(t, fx, '-.', t, dx, 'LineWidth', 2);
set(gca, 'FontSize', 12);
grid;
xticks([]);

subplot(3, 1, 1, 'Units', 'normalized', 'Position', [0.05, 0.65, 0.9, 0.25]);
plot(t, fx, '-.', t, dx, 'LineWidth', 2);
set(gca, 'FontSize', 12);
grid;
ylabel('m/s');
xticklabels([]);
legend('$f_{x1}$', '$\dot{p}_e$', 'interpreter', 'latex', 'FontSize', 18)

subplot(3, 1, 2, 'Units', 'normalized', 'Position', [0.05, 0.375, 0.9, 0.25]);
plot(t, fy,'-.', t, dy, 'LineWidth', 2);
grid;
xticklabels([]);
set(gca, 'FontSize', 12)
ylabel('m/s');
legend('$f_{x2}$', '$\dot{p}_n$', 'interpreter', 'latex', 'FontSize', 18)

subplot(3, 1, 3, 'Units', 'normalized', 'Position', [0.05, 0.1, 0.9, 0.25]);
plot(t, fz,'-.', t, dz, 'LineWidth', 2);
set(gca, 'FontSize', 12);
grid;
ylabel('m/s');
legend('$f_{x3}$', '-$\dot{p}_d$', 'interpreter', 'latex', 'FontSize', 18)
xlabel('Time (s)', 'FontSize', 15);
set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');

sgtitle('Velocities', 'FontSize', 15);

set(findall(gcf,'-property','FontWeight'), 'FontWeight','bold');

