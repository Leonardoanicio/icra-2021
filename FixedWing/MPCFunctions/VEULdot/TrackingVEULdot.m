function [X, U, E, Re, FInfo, Tout, t, DV, flag] = TrackingVEULdot(states, controls, MPC, ...
                                                            UAV, tf, dt, curve)
    Hqp = MPC.Hqp;
    Fqp = MPC.Fqp;
    S = MPC.S;
    T = MPC.T;
    nx = MPC.nx;
    nu = MPC.nu;
    options = MPC.options;
    
    %Configurações do campo
    tbeta = 0;
    circDir = 1; % recalculated below
    fstate = 'follow curve'; % recalculated below
    
    %Vetores para salvar os dos
    U = zeros(4, tf/dt);
    DV = zeros(4, tf/dt);
    E = zeros(4, tf/dt);
    Re = zeros(4, tf/dt);
    X = zeros(12, tf/dt);
    Tout = zeros(1, tf/dt);
    index = [7 8 9]; %phi theta psi
    
    %Informações do campo
    FInfo.nearo = zeros(1, tf/dt);
    FInfo.nearp = zeros(3, tf/dt);
    FInfo.state = zeros(1, tf/dt);
    FInfo.cirdir = zeros(1, tf/dt);
    FInfo.tbeta = zeros(1, tf/dt);
    FInfo.F = zeros(3, tf/dt);
    FInfo.Fc = zeros(3, tf/dt);
    FInfo.Fo = zeros(3, tf/dt);

    %Carrega os estados iniciais
    X(:, 1) = states;
    U(:, 1) = controls;
    xdot = zeros(3, 1);

    %Passa de vetor para celula
    states = num2cell(states);

    %Simulacao
    i = 1;
    dt = double(dt);
    tf = double(tf);
    for t=0:dt:tf
            [pn, pe, pd, u, v, w, phi, theta, psi, p, q, r] = deal(states{:});
            h = -pd;

            %Alpha, Beta, Va
            alpha = atan2(w,u);
            vt = norm([u,v,w]);
            beta = asin(v/vt);

            if i == 1
                alphadot = 0;
                alphaold = alpha;
            else
                alphadot = (alpha - alphaold)/dt;
                alphaold = alpha;
            end

            %Controles atuais
            ail = controls(1);
            elev = controls(2);
            rud = controls(3);

            %Calculo da densidade do ar
            temperatura = 288.15 - 0.0065*h;
            rho = 1.225*(temperatura/288.15)^(-9.81/(-0.0065*287.04)-1);

            %Estados atuais -> modelo linear
            x = [vt xdot(1) xdot(2) xdot(3)]';
            
            %Matrizes de forças
            Coeffs_C = coeffs_C(alpha, beta, vt, p, q, r, alphadot, rho, UAV);
            Coeffs_M = coeffs_M(alpha, beta, vt, ail, elev, rud, rho, UAV);
            Coeffs_P = (Coeffs_M'*Coeffs_M)\Coeffs_M';
            
            Vref = 23;
            [xob, fstate, tbeta, circDir, nearObstacle, nearPoint, F, Fc, Fo] = VEULdotHorizon(states, Vref, nx, dt,...
                                                           fstate, tbeta, circDir, curve);
              
            FInfo.F(:, i) = F;
            FInfo.Fc(:, i) = Fc;
            FInfo.Fo(:, i) = Fo;
            FInfo.circdir(i) = circDir;
            FInfo.tbeta(i) = tbeta;
            FInfo.nearo(i) = nearObstacle;
            FInfo.nearp(:, i) = nearPoint;
            switch fstate
                case 'follow curve'
                    FInfo.state(i) = 1;
                case 'transition to obstacle'
                    FInfo.state(i) = 2;
                case 'contour obstacle'
                    FInfo.state(i) = 3;
                case 'transition to curve'
                    FInfo.state(i) = 4;
            end
            
            tic;
            %Feedback Linearization
            [Alpha, Beta] = FLVEULdot(states, Coeffs_C, UAV);

            %Restricao dos estados
            x2 = [x; Beta\(Coeffs_M*controls - Alpha)];
            [Aeq, Beq, Aineq, Bineq] = VEULdotBounds(x2, T, S, Coeffs_P, Coeffs_M, Beta, controls, ...
                nx, nu, UAV.UpperBounds, UAV.LowerBounds);
            
            %Quadprog
            x = [x; zeros(4, 1)];
            f = 2*(x'*T' - xob')*Fqp; 
            [delta_v, fval, flag, output] = quadprog(Hqp, f, Aineq, Bineq, Aeq, Beq, [], [], zeros(4*nu, 1), options);
           
            Tout(i) = toc;
            
            if flag <= 0
                flag
                break
            end

            %Transforma para os controles
            controls = controls + Coeffs_P*Beta*delta_v(1:4);

            %Evolui o modelo
            xdot = EOM(states, Coeffs_M*controls + Coeffs_C, UAV);
            states_aux = cell2mat(states);
            states_aux = states_aux + xdot*dt;
            states = num2cell(states_aux);

            %Salva os dados
            i = i + 1;
            U(:, i) = controls;
            DV(:, i) = delta_v(1:4);
            X(:, i) = states_aux; %-> Modelo não linear

            %Atualiza as derivadas
            xdot = xdot(index);

            %Salva os erros
            E(:,i) = xob(1:4) - x(1:4);
            Re(:,i) = xob(1:4); 
            
            if mod(i, 10) == 0
                clc;
                fprintf('Simulating\n%.2fs', t);
            end
    end
    if flag > 0
        clc;
    end
    fprintf('Max Time = %.4f\n', max(Tout));
    fprintf('Mean Time = %.4f\n', mean(Tout));
end