function [X] = plotField(x, y, z, points, curveType)
    
    %Pega a curva
    [f_a1, f_a2, P, a_curve, b_curve, c_curve, O, kp] = getCurve(curveType);

    dx = (x(2) - x(1))/(points - 1);
    dy = (y(2) - y(1))/(points - 1);
    [xx, yy] = meshgrid(x(1):dx:x(2), y(1):dy:y(2));
    u = zeros(size(x));
    v = zeros(size(y));
    for i=1:size(xx, 1)
        for j=1:size(xx, 2)
            x = xx(i, j);
            y = yy(i, j);
            dx = curve_vector_alpha2([x; y; z], a_curve, b_curve, c_curve, f_a1, f_a2, P, kp);
            dx(1:2) = dx(1:2)/(1e-12 + norm(dx(1:2)));
            u(i, j) = dx(1);
            v(i, j) = dx(2);
        end
    end
    quiver(xx, yy, u, v);
    axis([min(min(xx)) max(max(xx)) min(min(yy)) max(max(yy))])
end