curve_type = 3;

ekf_data = true;

switch curve_type
    case 1
        if ekf_data
            x = load('C1EKF');
        else
            x = load('C1');
        end
    case 3
        if ekf_data
            x = load('C2EKF');
        else
            x = load('C2');
        end
end

x = struct2cell(x);
x = x{1};
% = [X, U, E, Re, FInfo, Tout, t, LinSys, flag, EKF_OUT]
X = x{1};
U = x{2};
E = x{3};
Re = x{4};
FInfo = x{5};