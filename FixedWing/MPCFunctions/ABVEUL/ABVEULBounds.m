function [Aeq, Beq, Aineq, Bineq] = ABVEULBounds(x, T, S, Coeffs_P, Coeffs_M, Beta, controls, ...
                                        nx, nu, UpperBounds, LowerBounds)   

    AEQ = (eye(6) - Coeffs_M*Coeffs_P)*Beta;
    Aeq = kron(eye(nu), AEQ);
   
    Beq = zeros(6*nu, 1);
    
    AINEQ1 = Coeffs_P*Beta;
    AINEQ1 = kron(tril(ones(nu, nu)), AINEQ1);
    
    BINEQ1U = repmat(UpperBounds - Coeffs_P*Coeffs_M*controls, nu, 1);
    BINEQ1L = repmat(LowerBounds - Coeffs_P*Coeffs_M*controls, nu, 1);
    
    AINEQ2 = kron(eye(nu), Coeffs_P*Beta);
    
    BINEQ2U = 0.01*ones(4*nu, 1);
    BINEQ2L = -0.01*ones(4*nu, 1);
    
    
    K = zeros(2, 15);
    K(1, 1) = 1;
    K(2, 3) = 1;
    K = kron(eye(nx), K);
    
    AINEQ3 = K*S;
    
    BINEQ3U = repmat([0.279; 30], nx , 1);
    BINEQ3U = BINEQ3U - K*T*x;
    BINEQ3L = repmat([-0.0349; 18], nx, 1);
    BINEQ3L = BINEQ3L - K*T*x;
    
    Aineq = [AINEQ1; -AINEQ1; AINEQ2; -AINEQ2; AINEQ3; -AINEQ3];
    Bineq = [BINEQ1U; -BINEQ1L; BINEQ2U; -BINEQ2L; BINEQ3U; -BINEQ3L];   
end