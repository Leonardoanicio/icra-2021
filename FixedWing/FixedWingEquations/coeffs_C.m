function C_6x1 = coeffs_C(alpha, beta, vt, p, q, r, alphadot, rho, UAV)
    L = (UAV.CLbeta*alpha + UAV.CL_0 + (UAV.CL_alphadot*alphadot + UAV.CLq*q)*UAV.Cw/(2*vt));
    CL = L;
    D = (UAV.CDbeta*beta + UAV.CD_0 + UAV.induced_drag*(CL - UAV.CD_mind)^2);
    Y = (UAV.CYbeta*beta + (UAV.CYp*p + UAV.CYr*r)*UAV.Bw/(2*vt));

    FWIND = 0.5*rho*vt^2*UAV.Sw*[-D; Y; -L];
    FAERO = (rotz(rad2deg(beta))'*roty(rad2deg(alpha)))'*FWIND;

    l = (UAV.Clbeta*beta + (UAV.Clp*p + UAV.Clr*r)*UAV.Bw/(2*vt))*UAV.Bw;
    m = (UAV.Cm0 + UAV.Cmbeta*alpha + (UAV.Cm_alphadot*alphadot + UAV.Cmq*q)*UAV.Cw/(2*vt))*UAV.Cw;
    n = (UAV.Cnbeta*beta + (UAV.Cnp*p + UAV.Cnr*r)*UAV.Bw/(2*vt))*UAV.Bw;

    MAERO = 0.5*rho*vt^2*UAV.Sw*[l; m; n];
    r_cross = (UAV.r_aero_abc - UAV.r_cg_abc)';

    M2 = cross(r_cross, FAERO);

    FORCES = FAERO;
    MOMENTS = MAERO + M2;
    C_6x1 = vertcat(FORCES, MOMENTS);
end